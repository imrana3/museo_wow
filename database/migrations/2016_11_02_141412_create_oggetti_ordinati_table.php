<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOggettiOrdinatiTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('oggetti_ordinati', function (Blueprint $table) {
            $table->increments('id');

            $table->integer('id_ordine')->unsigned()->index();
            $table->integer('id_oggetto')->unsigned()->index();
            $table->integer('id_fornitore')->unsigned()->index();
            $table->integer('quantita')->unsigned()->default(1);

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::statement('SET FOREIGN_KEY_CHECKS = 0');
        Schema::drop('oggetti_ordinati');
        DB::statement('SET FOREIGN_KEY_CHECKS = 1');
    }
}
