<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOrdiniTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ordini', function (Blueprint $table) {
            $table->increments('id');

            $table->string('nome');
            $table->integer('id_utente')->unsigned()->index();
            $table->timestamp('data');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::statement('SET FOREIGN_KEY_CHECKS = 0');
        Schema::drop('ordini');
        DB::statement('SET FOREIGN_KEY_CHECKS = 1');
    }
}
