<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFornitoriOggettoTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('fornitori_oggetto', function (Blueprint $table) {
            $table->increments('id');

            $table->integer('id_oggetto')->unsigned()->index();
            $table->integer('id_fornitore')->unsigned()->index();

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::statement('SET FOREIGN_KEY_CHECKS = 0');
        Schema::drop('fornitori_oggetto');
        DB::statement('SET FOREIGN_KEY_CHECKS = 1');
    }
}
