<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOggettiTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('oggetti', function (Blueprint $table) {
            $table->increments('id');

            $table->string('barcode')->nullable();
            $table->string('nome');
            $table->longText('descrizione');
            $table->integer('quantita')->unsigned();

            $table->string('produttore')->nullable();
            $table->integer('id_libro')->unsigned()->index()->nullable();

            $table->string('luogo_giacenza');
            $table->float('prezzo_acquisto');
            $table->float('prezzo_vendita');
            $table->float('iva')->unsigned()->default(0);

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::statement('SET FOREIGN_KEY_CHECKS = 0');
        Schema::drop('oggetti');
        DB::statement('SET FOREIGN_KEY_CHECKS = 1');
    }
}
