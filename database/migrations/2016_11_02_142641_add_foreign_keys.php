<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddForeignKeys extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('ordini', function($table) {

            $table->foreign('id_utente')
                ->references('id')->on('users')
                ->onDelete('cascade');
        });

        Schema::table('vendite', function($table) {

            $table->foreign('id_utente')
                ->references('id')->on('users')
                ->onDelete('cascade');

        });

        Schema::table('oggetti', function($table) {

            $table->foreign('id_libro')
                ->references('id')->on('libri')
                ->onDelete('cascade');

        });

        Schema::table('oggetti_ordinati', function($table) {

            $table->foreign('id_ordine')
                ->references('id')->on('ordini')
                ->onDelete('cascade');

            $table->foreign('id_oggetto')
                ->references('id')->on('oggetti')
                ->onDelete('cascade');

            $table->foreign('id_fornitore')
                ->references('id')->on('fornitori')
                ->onDelete('cascade');

        });

        Schema::table('fornitori_oggetto', function($table) {

            $table->foreign('id_oggetto')
                ->references('id')->on('oggetti')
                ->onDelete('cascade');

            $table->foreign('id_fornitore')
                ->references('id')->on('fornitori')
                ->onDelete('cascade');

        });

        Schema::table('oggetti_venduti', function($table) {

            $table->foreign('id_vendita')
                ->references('id')->on('vendite')
                ->onDelete('cascade');

            $table->foreign('id_oggetto')
                ->references('id')->on('oggetti')
                ->onDelete('cascade');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
