
<!-- delete modal-->
<div id="deleteModal" class="modal fade" role="dialog">
    <div class="modal-dialog modal-sm">

        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Confermi eliminazione?</h4>
            </div>
            <div class="modal-footer">
                <button class="btn btn-default" data-dismiss="modal">Annulla</button>
                <button id="conferma" class="btn btn-success" data-dismiss="modal">Elimina</button>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
    function conferma(id, url) {
        $("#conferma").attr("onclick","elimina("+id+", "+url+")");
        $('#deleteModal').modal('show');
    }

    function elimina(id, url) {
        $.ajax({
            data: {
                "_token": "{{ csrf_token() }}",
            },
            url: url + id,
            type: 'DELETE',
            success: function(result) {
                if(result == "success"){
                    message = '<div class="alert alert-success">'+
                            '<h4><strong>Fatto!</strong> Eliminazione effettuata con successo.<br>' +
                            'Redirect in corso, se non succede nulla <a href="'+url+'">clicca qui</a></h4>'+
                            '</div>'
                    $("#placeholder").html(message);
                }else {
                    message = '<div class="alert alert-danger">'+
                            '<h4><strong>Errore!</strong>'+
                            'Redirect in corso, se non succede nulla <a href="'+url+'">clicca qui</a></h4>'+
                            '</div>';
                    $("#placeholder").html(message);
                }

                setTimeout(function(){ window.location = url; }, 2000);
            }
        });
    }
</script>