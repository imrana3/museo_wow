@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-6 col-md-offset-3">
                @if(Session::has('success'))
                    <div class="alert alert-success">
                        <strong>Fatto! </strong>{{Session::get('success')}}
                        <strong><a href="/utenti/{{$utente->id}}">Torna al profilo</a></strong>
                    </div>
                @endif
                <div class="panel panel-warning">
                    <div class="panel-heading">Modifica dati</div>
                    <div class="panel-body">
                        <form class="form-horizontal" role="form" method="POST" action="/utenti/modifica/{{$utente->id}}">
                            {{ csrf_field() }}
                            <input name="_method" type="hidden" value="PUT">
                            <div class="form-group{{ $errors->has('nome') ? ' has-error' : '' }}">
                                <label for="name" class="col-md-4 control-label">Nome</label>
                                <div class="col-md-6">
                                    <input id="nome" type="text" class="form-control" name="nome" value="{{ $utente->nome or old('nome') }}">

                                    @if ($errors->has('nome'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('nome') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group{{ $errors->has('cognome') ? ' has-error' : '' }}">
                                <label for="cognome" class="col-md-4 control-label">Cognome</label>
                                <div class="col-md-6">
                                    <input id="cognome" type="text" class="form-control" name="cognome" value="{{ $utente->cognome or old('cognome') }}">

                                    @if ($errors->has('cognome'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('cognome') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group{{ $errors->has('telefono') ? ' has-error' : '' }}">
                                <label for="telefono" class="col-md-4 control-label">Telefono</label>
                                <div class="col-md-6">
                                    <input id="telefono" type="text" class="form-control" name="telefono" value="{{ $utente->telefono or old('telefono') }}">

                                    @if ($errors->has('telefono'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('telefono') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                                <label for="email" class="col-md-4 control-label">E-Mail</label>
                                <div class="col-md-6">
                                    <input id="email" type="email" class="form-control" name="email" value="{{ $utente->email or old('email') }}">

                                    @if ($errors->has('email'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="col-md-8 col-md-offset-6">
                                    <a href="/utenti/{{$utente->id}}" class="btn btn-danger">Annulla</a>
                                    <button type="submit" class="btn btn-warning">
                                        <i class="fa fa-btn fa-user"></i> Modifica
                                    </button>
                                </div>
                            </div>

                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
