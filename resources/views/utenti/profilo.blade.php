@extends('layouts.app')
@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="row">
                <div class="col-md-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h3>{{$user->nome}} {{$user->cognome}}</h3><h6>
                                <span>{{$user->email}}</span><br>
                                <span>{{$user->telefono}}</span><br>
                                <span>{{$user->tipologia}}</span><br>
                                <span>utenti dal {{$user->created_at}}</span><br>
                            </h6>

                        </div>

                    </div>

                </div>
                </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="panel panel-default">
                                <div class="panel-heading"><h4>Vendite effettuate ({{$vendite->total()}})</h4></div>
                                <div class="panel-body" id="tabella_vendite">
                                    @include('utenti.tabella_vendita')
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="panel panel-default">
                                <div class="panel-heading"><h4>Ordini effettuati ({{$ordini->total()}})</h4></div>
                                <div class="panel-body" id="tabella_ordini">
                                    @include('utenti.tabella_ordini')
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
    function paginate(page, src){

        var query_string = $("#search_"+src).val();
        var query_url = '/utenti/{{$user->id}}';
        $.ajax({
            url: query_url,
            type: 'GET',
            data : {
            src: src,
            page: page
            },
            success: function( data ){
                console.log(data);
            $("#tabella_"+src).html(data.tabella);
            }
        });
    }


    $(document).on('click','.pagination a', function(e){
        e.preventDefault();
        var src = $(this).attr('href').split('src=')[1];
        src = src.split('&')[0];
        var page = $(this).attr('href').split('page=')[1];
        paginate(page, src);

});
</script>
@endsection