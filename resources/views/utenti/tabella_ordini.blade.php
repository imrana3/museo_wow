@if(count($ordini) > 0)
    <table id="ordini_table" class="table table-hover">
        <thead>
        <tr>
            <th>Data</th>
            <th>Nome</th>
            <th>Oggetti</th>
        </tr>
        </thead>
        <tbody>
        @foreach($ordini as $ordine)
            <tr style="cursor: pointer">
                <td  onclick="window.location='/ordini/{{$ordine->id}}'">
                    {{date('d F Y', strtotime($ordine->data))}}<br>
                    alle {{date('H:i', strtotime($ordine->created_at))}}
                </td>
                <td onclick="window.location='/ordini/{{$ordine->id}}'">{{$ordine->nome}}</td>
                <? $oggetti = $ordine->oggetti ?>
                <td>
                    <span class="btn btn-sm btn-default" data-toggle="collapse" data-target="#ordine_lista{{$ordine->id}}">oggetti</span>
                    <div id="ordine_lista{{$ordine->id}}" class="collapse">
                        <ul>
                            @foreach($oggetti as $oggetto)
                                <li><a href="/oggetti/{{$oggetto->id}}">{{$oggetto->nome}}</a>  {{$oggetto->pivot->quantita}}x</li>
                            @endforeach
                        </ul>
                    </div>
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
    <div class="pagination">{{$ordini->appends('src', 'ordini')->links()}}</div>
@else
    <div><h4>Non è presente alcun ordine.</h4></div>
@endif