@extends('layouts.app')
@section('content')
<div class="container">
    <p><a href="/admin/registra"  class="btn btn-success btn-lg">Registra utente</a></p>
    <div id="placeholder">
        @if(Session::has('success'))
            <div class="alert alert-success">
                <strong>Fatto! </strong>{{Session::get('success')}}
            </div>
        @endif
    </div>
    <div class="panel panel-default">
        <div class="panel-body">
            <div class="input-group col-lg-6">
                <input onkeyup="listen()"type="text"  id="search"  class="form-control" placeholder="Cerca utente..." autofocus value="{{$query or ""}}">
                <span class="input-group-btn">
                    <span onclick="cerca()" class="btn btn-success btn-md">Cerca</span>
                    @if(isset($query))
                        <a href="/admin" class="btn btn-danger btn-md">Annulla ricerca</a>
                    @endif
                </span>
            </div><br>

            @if(isset($query))
                @if(count($utenti) == 0)
                    <div><h3>Nessun risultato.</h3></div>
                @else
                    <div><h4>Trovati {{$utenti->total()}} risultati per <b>{{$query}}</b></h4></div>
                @endif
            @endif

            <div id="lista_utenti">
                @foreach(array_chunk($utenti->all(), 4)as $row)
                    <div class="row">
                        @foreach($row as $utente)
                            <div class="col-md-6 col-sm-12 col-lg-3"  style="cursor: pointer;">
                                <div class="panel panel-success">
                                    <div class="panel-heading" onclick="window.location='/utenti/{{$utente->id}}'">
                                        <h4>{{$utente->nome}} {{$utente->cognome}}</h4>
                                    </div>
                                    <div class="panel-body">
                                        <span onclick="window.location='/utenti/{{$utente->id}}'">
                                            <div>
                                            <span class="glyphicon glyphicon-envelope"></span> <a href="mailto:{{$utente->email}}">{{$utente->email}}</a><br>
                                            <span class="glyphicon glyphicon-phone"></span> {{$utente->telefono}}<br>
                                            <span class="glyphicon glyphicon-user"></span> {{$utente->username}}<br>
                                            <span class="glyphicon glyphicon-user"></span> {{$utente->tipologia}}
                                        </div><br>
                                        </span>
                                        <div class="btn-group pull-right">
                                            <a href="/utenti/modifica/{{$utente->id}}" class="btn btn-default btn-sm">modifica</a>
                                            <span onclick="conferma('{{$utente->id}}', '/admin/')" class="btn btn-default btn-sm">elimina</span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        @endforeach
                    </div>
                @endforeach

                @if(count($utenti) > 0)
                    <div class="pagination">{{$utenti->links()}}</div>
                @endif
            </div>

        </div>
    </div>
</div>

@include('common.elimina_modal');
<script type="text/javascript">
    function cerca()
    {
        var query_string = $("#search").val();
        if(query_string.length > 0) {
            var query_url = '/admin/cerca/' + query_string;
            location.href = query_url;
        }
    }

    function listen() {
        //se preme Invio
        if(event.keyCode == 13)
            cerca();
    }

</script>
@endsection