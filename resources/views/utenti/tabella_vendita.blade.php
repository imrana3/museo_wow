@if(count($vendite) > 0)
    <table id="vendite_table" class="table table-hover">
        <thead>
        <tr>
            <th>Data</th>
            <th>Oggetti</th>
        </tr>
        </thead>
        <tbody>
        @foreach($vendite as $vendita)
            <tr style="cursor: pointer">
                <td  onclick="window.location='/vendite/{{$vendita->id}}'">
                    {{date('d F Y', strtotime($vendita->created_at))}}<br>
                    alle {{date('H:i', strtotime($vendita->created_at))}}
                </td>

                <? $oggetti = $vendita->oggetti ?>
                <td>
                    <span class="btn btn-sm btn-default" data-toggle="collapse" data-target="#lista{{$vendita->id}}">oggetti</span>
                    <div id="lista{{$vendita->id}}" class="collapse">
                        <ul>
                            @foreach($oggetti as $oggetto)
                                <li><a href="/oggetti/{{$oggetto->id}}">{{$oggetto->nome}}</a>  {{$oggetto->pivot->quantita}}x</li>
                            @endforeach
                        </ul>
                    </div>
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
    <div class="pagination">{{$vendite->appends('src', 'vendite')->links()}}</div>
@else
    <div><h4>Non è presente alcuna vendita.</h4></div>
@endif