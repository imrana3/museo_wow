@extends('layouts.app')
@section('content')
<link href="https://fonts.googleapis.com/css?family=Lato:100" rel="stylesheet" type="text/css">

<style>


    .all{
        height: 100%;

        margin: 0;
        padding: 0;
        width: 100%;
        color: #000;
        font-weight: 100;

        text-align: center;
        vertical-align: middle;

        display: inline-block;

        margin-bottom: 40px;
    }
    #maindiv{
        background-color: rgba(255, 255, 255, 0.5);
    }
    #title {
        font-size: 72px;
    }
    #back {
         font-size: 80px;
     }
</style>
<div class="col-lg-8 col-lg-offset-2 col-md-8 col-md-offset-2 col-sm-8 col-sm-offset-2" id="maindiv">
    <div id="title" class="all">
        @if(isset($message))
            {{$message}}
        @else
            Accesso non autorizzato.
        @endif
    </div>
    <div id="back" class="all">
        <a href="/">HOME</a>
    </div>
</div>
@endsection