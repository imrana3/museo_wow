@extends('layouts.app')
@section('content')
<div class="container ">
    <p><button onclick="window.location='/fornitori/nuovo'"  type="button" class="btn btn-success btn-lg">Inserisci nuovo</button></p>
    <div id="placeholder"></div>
    <div class="panel panel-default">
        <div class="panel-body">
            <div class="col-lg-12">
                <div class="input-group col-lg-6">
                    <input onkeyup="listen()"type="text"  id="search"  class="form-control" placeholder="Cerca fornitore..." autofocus value="{{$query or ""}}">
                    <span class="input-group-btn">
                        <a onclick="cerca()" href="#" class="btn btn-success btn-md">Cerca</a>
                        @if(isset($query))
                            <a href="/fornitori" class="btn btn-danger btn-md">Annulla ricerca</a>
                        @endif
                    </span>
                </div>
                <div class="input-group col-lg-2 col-lg-offset-4 col-md-6 col-sm-6 row">
                    <select id="sort" onchange="sort()" class="form-control" id="fornitore">
                        <option value="created_at">Ordina per data</option>
                        <option value="nome">Ordina per nome</option>
                    </select>
                    <br><br><br>
                </div>
            </div>

            @if(isset($query))
                @if(count($fornitori) == 0)
                    <div><h3>Nessun risultato.</h3></div>
                @else
                    <div><h4>Trovati {{$fornitori->total()}} risultati per <b>{{$query}}</b></h4></div>
                @endif
            @else
                @if(count($fornitori) == 0)
                    <div><h3>Non è presente alcun fornitore.</h3> <a href="/fornitori/nuovo" class="btn btn-success btn-md">Inserisci ora</a> </div>
                @endif
            @endif

            <div id="lista_fornitori">
                    @foreach(array_chunk($fornitori->all(), 4)as $row)
                    <div class="row">
                        @foreach($row as $fornitore)
                        <div class="col-md-6 col-sm-12 col-lg-3"  style="cursor: pointer;">
                            <div class="panel panel-success">
                                <div class="panel-heading" onclick="window.location='/fornitori/{{$fornitore->id}}'">
                                    <h4>{{$fornitore->nome}}</h4>
                                </div>
                                <div class="panel-body">
                                    <span onclick="window.location='/fornitori/{{$fornitore->id}}'">
                                    @if($fornitore->alias)
                                        <? $aliases = explode(",", $fornitore->alias); ?>
                                        <span class="glyphicon glyphicon-user"></span>
                                        @foreach($aliases as $alias)
                                            <span class="label label-primary">{{$alias}}</span>
                                        @endforeach
                                        <br>
                                    @endif
                                    <div>
                                        <span class="glyphicon glyphicon-envelope"></span> <a href="mailto:{{$fornitore->email}}">{{$fornitore->email}}</a><br>
                                        <span class="glyphicon glyphicon-phone"></span> {{$fornitore->telefono}}<br>
                                        <span class="glyphicon glyphicon-home"></span> {{$fornitore->indirizzo}}<br>
                                        <span class="glyphicon glyphicon-file"></span> {{$fornitore->tipo_contratto}}
                                    </div><br>
                                    </span>
                                    <div class="btn-group pull-right">
                                        <a href="/fornitori/modifica/{{$fornitore->id}}" class="btn btn-default btn-sm">modifica</a>
                                        <span onclick="conferma('{{$fornitore->id}}', '/fornitori/')" class="btn btn-default btn-sm">elimina</span>
                                    </div>
                                </div>
                            </div>
                        </div>
                        @endforeach
                    </div>
                    @endforeach
                @if(count($fornitori) > 0)
                    <div class="pagination">{{$fornitori->appends("sort", $sortBy)->links()}}</div>
                @endif
        </div>
    </div>
    </div>
</div>

@include('common.elimina_modal');
<script type="text/javascript">
    function cerca()
    {

        var type = $('#sort').val();
        var query_string = $("#search").val();
        if(query_string.length > 0) {
            var query_url = '/fornitori/cerca/' + query_string + "?sort="+type;
            location.href = query_url;
        }
    }

    function listen() {
        //se preme Invio
        if(event.keyCode == 13)
            cerca();

        var query_string = $("#search").val();
        if(query_string.length == 0)
            location.href = "/fornitori/";
    }

    function sort() {
        var type = $('#sort').val();
        var url = document.location.href;
        var url = url.split("?")[0] +"?sort=" + type;
        $(window.location).attr('href', url);

    }

    $( document ).ready(function() {
        $('#sort').val('{{$sortBy}}').prop('selected', true);
    });
</script>
@endsection