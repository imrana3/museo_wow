
    <div class="form-group{{ $errors->has('nome') ? ' has-error' : '' }}">
        <label for="nome" class="col-md-4 control-label">Nome</label>
        <div class="col-md-6">
            <input id="nome" type="text" class="form-control" name="nome" value="{{ isset($fornitore) ? $fornitore['nome'] : old('nome') }}" autofocus>
            @if ($errors->has('nome'))
                <span class="help-block">
                    <strong>{{ $errors->first('nome') }}</strong>
                </span>
            @endif
        </div>
    </div>

    <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
        <label for="email" class="col-md-4 control-label">Email</label>
        <div class="col-md-6">
            <input id="email" type="email" class="form-control" name="email" value="{{ isset($fornitore) ? $fornitore['email'] :  old('email') }}">
            @if ($errors->has('email'))
                <span class="help-block">
                    <strong>{{ $errors->first('email') }}</strong>
                </span>
            @endif
        </div>
    </div>

    <div class="form-group{{ $errors->has('telefono') ? ' has-error' : '' }}">
        <label for="telefono" class="col-md-4 control-label">Telefono</label>
        <div class="col-md-6">
            <input id="telefono" type="number" class="form-control" name="telefono" value="{{ isset($fornitore) ? $fornitore['telefono'] : old('telefono') }}">
            @if ($errors->has('telefono'))
                <span class="help-block">
                    <strong>{{ $errors->first('telefono') }}</strong>
                </span>
            @endif
        </div>
    </div>

    <div class="form-group{{ $errors->has('indirizzo') ? ' has-error' : '' }}">
        <label for="indirizzo" class="col-md-4 control-label">Indirizzo</label>
        <div class="col-md-6">
            <input id="indirizzo" type="text" class="form-control" name="indirizzo" value="{{ isset($fornitore) ? $fornitore['indirizzo'] :  old('indirizzo') }}">
            @if ($errors->has('indirizzo'))
                <span class="help-block">
                    <strong>{{ $errors->first('indirizzo') }}</strong>
                </span>
            @endif
        </div>
    </div>

    <div class="form-group{{ $errors->has('tipo_contratto') ? ' has-error' : '' }}">
        <label for="tipo_contratto" class="col-md-4 control-label">Tipologia contratto</label>
        <div class="col-md-6">
            <input id="tipo_contratto" type="text" class="form-control" name="tipo_contratto" value="{{ isset($fornitore) ? $fornitore['tipo_contratto'] :  old('tipo_contratto') }}">
            @if ($errors->has('tipo_contratto'))
                <span class="help-block">
                    <strong>{{ $errors->first('tipo_contratto') }}</strong>
                </span>
            @endif
        </div>
    </div>

    <div class="form-group{{ $errors->has('alias') ? ' has-error' : '' }}">
        <label for="alias" class="col-md-4 control-label">Atri nomi</label>
        <div class="col-md-6">
            <input id="alias" type="text" class="form-control" name="alias" value="{{ isset($fornitore) ? $fornitore['alias'] :  old('alias') }}" placeholder="usare la virgola come separatore">
            @if ($errors->has('alias'))
                <span class="help-block">
                    <strong>{{ $errors->first('alias') }}</strong>
                </span>
            @endif
        </div>
    </div>