@extends('layouts.app')
@section('content')
    <div class="container">
        <div class="row col-lg-6 col-lg-offset-3 col-md-6  col-md-offset-3">
            @if(Session::has('success'))
                <div class="alert alert-success">
                    <strong>Fatto! </strong>{{Session::get('success')}}
                </div>
            @endif
            <div class="panel panel-warning">
                <div class="panel-heading">Modifica dati fornitore</div>
                <div class="panel-body">
                    <form class="form-horizontal" role="form" method="POST" action="{{ url('/fornitori/aggiorna/'.$fornitore->id.'') }}">
                        {{ csrf_field() }}
                        <input name="_method" type="hidden" value="PUT">
                        @include('fornitori.fornitore_form')
                        <div class="form-group">
                            <div class="col-md-8 col-md-offset-4 pull-right">
                                <span  onclick="window.location='/fornitori/{{$fornitore->id}}'" class="btn btn-danger">Annulla</span>
                                <button type="submit" class="btn btn-success">Modifica</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection