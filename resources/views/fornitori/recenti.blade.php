{{--Ultimi x fornitori inseriti--}}
<div class="col-md-6 col-sm-6 col-lg-6"  style="cursor: pointer;">
    <div class="panel panel-default">
        <div class="panel-heading">
            <div class="btn-group pull-right">
                <a href="/fornitori/" class=" btn btn-sm btn-primary gl-plus">visualizza tutti</a>
                <a href="/fornitori/nuovo" class="btn btn-sm btn-success gl-plus">nuovo</a>
            </div>
            <h5>Nuovi fornitori</h5>
        </div>
        <div class="panel-body">
            <table class="table table-hover">
                <thead><th>Data</th><th>Nome</th><th>Telefono</th></thead>
                <tbody>
                @foreach($fornitori as $fornitore)
                    <tr onclick="window:location='/fornitori/{{$fornitore->id}}'" data-toggle="tooltip" data-delay='{"show":"3000"}' title="clicca per visualizzare i dettagli del fornitori">
                        <td>
                            {{date('d F Y', strtotime($fornitore->created_at))}}<br>
                            alle {{date('H:i', strtotime($fornitore->created_at))}}
                        </td>
                        <td><b>{{$fornitore->nome}}</b></td>
                        <td>{{$fornitore->telefono}}</td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>