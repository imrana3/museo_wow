@extends('layouts.app')
@section('content')
<div class="container">
    <div id="placeholder"></div>
    <div class="panel panel-default">
        <div class="panel-heading">
            <h1>{{$fornitore->nome}}</h1>
            <div class="btn-group pull-right">
                <a href="/fornitori/modifica/{{$fornitore->id}}" class="btn btn-warning btn-md">modifica</a>
                <span onclick="conferma('{{$fornitore->id}}', '/fornitori/')" class="btn btn-danger btn-md">elimina</span>

            </div>

            <h5>
                {{date('d F Y', strtotime($fornitore->created_at))}}<br>
                alle {{date('H:i', strtotime($fornitore->created_at))}}
            </h5>
        </div>
        <div class="panel-body">
            <div class="col-lg-6 col-md-6">
                @if($fornitore->alias)
                    <? $aliases = explode(",", $fornitore->alias); ?>
                    <h4><span class="glyphicon glyphicon-user"></span>
                        @foreach($aliases as $alias)
                            <span class="label label-default">{{$alias}}</span>
                        @endforeach
                    </h4>
                @endif
                <h4><span class="glyphicon glyphicon-envelope"></span> {{$fornitore->email}}</h4>
                <h4><span class="glyphicon glyphicon-phone"></span> {{$fornitore->telefono}}</h4>
            </div>
            <div class="col-lg-6 col-md-6">

                <h4><span class="glyphicon glyphicon-home"></span> {{$fornitore->indirizzo}}</h4>
                <h4><span class="glyphicon glyphicon-file"></span> {{$fornitore->tipo_contratto}}</h4>
            </div>
        </div>
    </div>


    <div class="row">
        <div class="col-md-4">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h5>Oggetti forniti</h5>
                    <h6>Lista degli oggetti che sono forniti da {{$fornitore->nome}}</h6>
                </div>
                <div class="panel-body">
                    @if(count($oggetti_forniti) ==  0)
                        <div><h4>Nessun oggetto presente.</h4></div>
                    @else
                        <table id="gadget_table" class="table table-hover ">
                            <thead>
                            <tr>
                                <th>Nome</th>
                                <th>Descrizione</th>
                                <th>Azioni veloci</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($oggetti_forniti as $oggetto)
                                <tr style="cursor: pointer">
                                    <td onclick=" window.location='/oggetti/{{$oggetto->id}}'" >{{$oggetto->nome}}</td>
                                    <td onclick=" window.location='/oggetti/{{$oggetto->id}}'" >{{str_limit($oggetto->descrizione, 150)}}</td>
                                    <td>
                                        <a href="/ordini/nuovo/?q={{$oggetto->nome}}" class="btn btn-success btn-sm glyphicon glyphicon-plus"></a>
                                        <a href="/vendite/nuova?q={{$oggetto->nome}}" class="btn btn-info btn-sm glyphicon glyphicon-euro"></a>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    @endif
                </div>
            </div>
        </div>
        <div class="col-md-8">
            <div class="panel panel-default ">
                <div class="panel-heading">
                    <h5>Storico ordini</h5>
                    <h6>Lista di oggetti che sono stati ordinati da questo fornitore.</h6>
                </div>
                <div class="panel-body">
                    @if(count($oggetti) == 0 )
                        <div>
                           <h4>Non è presente alcun ordine in cui sia coinvolto questo fornitore.</h4>
                            <a href="/ordini/nuovo/" class="btn btn-success btn-md">effettua ordina</a>
                        </div>
                    @else
                    <h6>Sono presenti {{$oggetti->total()}} oggetti.</h6>
                    <table class="table table-hover" id="lista_oggetti">
                        <thead>
                        <tr>
                            <th>Data ordine</th>
                            <th>Ordine</th>
                            <th>Oggetto</th>
                            <th>Quantita</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($oggetti as $oggetto)
                            <? $ordine = \App\Ordine::find($oggetto->pivot->id_ordine);?>
                            <tr style="cursor: pointer" onclick="window.location='/ordini/{{$ordine->id}}'">
                                <td>{{date('d F Y', strtotime($oggetto->created_at))}}<br>
                                    alle {{date('H:i', strtotime($oggetto->created_at))}}</td>
                                <td><a href="/ordini/{{$ordine->id}}">{{$ordine->nome}}</a></td>
                                <td><a href="/oggetti/{{$oggetto->id}}">{{$oggetto->nome}}</a></td>
                                <td>{{$oggetto->pivot->quantita}}</td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                    <div class="pagination">{{$oggetti->fragment('lista_oggetti')->links()}}</div>
                    @endif
                </div>
            </div>
        </div>
    </div>

@include('common.elimina_modal')
</div>
@endsection