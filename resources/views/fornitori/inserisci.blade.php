@extends('layouts.app')
@section('content')
    <div class="container">
        <div class="row col-lg-6 col-lg-offset-3 col-md-6  col-md-offset-3">
            @if(Session::has('success'))
                <div class="alert alert-success">
                    <strong>Fatto! </strong>{{Session::get('success')}}
                </div>
            @endif
            <div class="panel panel-success">
                <div class="panel-heading">Inserisci nuovo fornitore</div>
                <div class="panel-body">
                    <form class="form-horizontal" role="form" method="POST" action="{{ url('/fornitori/nuovo') }}">
                        {{ csrf_field() }}

                        @include('fornitori.fornitore_form')
                        <div class="form-group">
                            <div class="col-md-6 col-md-offset-4 pull-right">
                                <span  onclick="window.location='/fornitori/'" class="btn btn-danger">Annulla</span>
                                <button type="submit" class="btn btn-success"> Inserisci</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection