{{--Ultime x vendite--}}
<div class="col-md-6 col-sm-6 col-lg-6">
    <div class="panel panel-default">
        <div class="panel-heading">
            <div class="btn-group pull-right">
                <a href="/vendite/" class=" btn btn-sm btn-primary gl-plus">visualizza tutte</a>
                <a href="/vendite/nuova" class="btn btn-sm btn-success gl-plus">nuova</a>
            </div>
            <h5>Ultime vendite</h5>
        </div>
        <div class="panel-body">
            <table class="table table-hover"  style="cursor: pointer;">
                <thead><th>Data</th><th>Oggetti</th></thead>
                <tbody>
                @foreach($vendite as $vendita)
                    <tr onclick="window:location ='/vendite/{{$vendita->id}}'" data-toggle="tooltip" data-delay='{"show":"3000"}' title="clicca per visualizzare i dettagli della vendita">
                        <? $oggetti_vendita = $vendita->oggetti()->get(); ?>
                        <td>
                            {{date('d F Y', strtotime($vendita->data))}}<br>
                            alle {{date('H:i', strtotime($vendita->created_at))}}
                        </td>
                        <td>
                            <div>
                                @foreach($oggetti_vendita as $oggetto)
                                    <a href="/oggetti/{{$oggetto->id}}">{{$oggetto->nome}}</a>  {{$oggetto->pivot->quantita}}x,
                                @endforeach
                            </div>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>