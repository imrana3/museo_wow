@extends('layouts.app')
@section('content')
<div class="container">
    <link rel="stylesheet" href="/css/datepicker.css">
    <p><span onclick="window.location='/vendite/nuova'"  type="button" class="btn btn-success btn-lg">Nuova vendita</span></p>
    <div id="placeholder"></div>
    <div class="panel panel-default">
        <div class="panel-body">
            <div class="input-group col-lg-6">
                <input value="{{$q['q'] or ""}}" onkeyup="listen()"type="text"  id="search"  class="form-control" placeholder="Cerca oggetto venduto..." autofocus>
                <span class="input-group-btn">
                    <a onclick="cerca()" href="#" class="btn btn-success btn-md">Cerca</a>
                    @if(!empty($q))
                        <a href="/vendite" class="btn btn-danger btn-md">Annulla ricerca</a>
                    @endif
                </span>
            </div>
            <p class="input-group date input-daterange col-lg-6">
                <input value="{{$q['dal'] or ""}}" id="data_inizio" type="text" class="form-control" placeholder="Dal..">
                <span class="input-group-addon">-</span>
                <input value="{{$q['al']  or ""}}" id="data_fine" type="text" class="form-control" placeholder="Al..">
                <div id="error_placeholder" style="display: none" class="alert alert-danger" role="alert">...</div>
            </p>

        </div>
        <br>
        @if(!empty($q))
            <div class="container">
                @if($q['q'] != "")
                    <h4>Termine di ricerca: <b>{{$q['q']}}</b></h4>
                @endif
                @if(isset($q['dal']) || isset($q['al']))
                    <h5>Vendite effettuate
                        @if(isset($q['dal']))
                            dal <b>{{$q['dal']}}</b>
                        @endif

                        @if(isset($q['al']) )
                            fino al <b>{{$q['al']}}</b>
                        @endif
                    </h5>
                @endif
                @if(count($vendite) == 0)
                    <h3>Nessuna vendita soddisfa i cretieri di ricerca.</h3>
                @else
                    <h4>{{$vendite->total()}} risultati trovati.</h4>
                @endif
            </div>
        @endif
        @if(count($vendite) > 0)
            <table id="vendite_table" class="table table-hover">
                <thead>
                <tr>
                    <th>Azioni</th>
                    <th>Data</th>
                    <th>Oggetti</th>
                    <th>Effettuato da</th>
                </tr>
                </thead>
                <tbody>
                @foreach($vendite as $vendita)
                    <tr style="cursor: pointer">
                        <td>
                            <span onclick="conferma('{{$vendita->id}}', '/vendite/')" class="btn btn-danger btn-sm glyphicon glyphicon-trash"></span>
                        </td>
                        <td  onclick="window.location='/vendite/{{$vendita->id}}'">
                            {{date('d F Y', strtotime($vendita->created_at))}}<br>
                            alle {{date('H:i', strtotime($vendita->created_at))}}
                        </td>
                        <td>
                            <? $oggetti = $vendita->oggetti ?>
                            <ul>
                            @foreach($oggetti as $oggetto)
                                <li><a href="/oggetti/{{$oggetto->id}}">{{$oggetto->nome}}</a> {{$oggetto->pivot['quantita']}}x  € {{$oggetto->pivot['prezzo_applicato']}}</li>
                            @endforeach
                            </ul>
                        </td>
                        <td><a href="/utenti/{{$vendita->user->id}}">{{$vendita->user->nome}}</a></td>
                    </tr>
                @endforeach
                </tbody>
            </table>
            @if(!empty($q))
                <div class="pagination">{{$vendite->appends($q)->links()}}</div>
            @else
                <div class="pagination">{{$vendite->links()}}</div>
            @endif
        @elseif(!isset($q))
            <div><h3>Non è presente alcuna vendita.</h3></div>
        @endif
    </div>
</div>
</div>

@include('common.elimina_modal')

<script src="/js/datepicker.js"></script>
<script type="text/javascript">
    data_inizio = "";
    data_fine = "";
    function cerca()
    {
        var data_inizio = $('#data_inizio').val();
        var data_fine = $('#data_fine').val();

        var query_string = $("#search").val();

        var query_url = '/vendite/cerca?q=' + query_string;
        if (controllaData(data_inizio))
            query_url += "&dal=" + data_inizio;
        if (controllaData(data_fine))
            query_url += "&al=" + data_fine;

        location.href = query_url;

    }

    function controllaData(data){
        if(data.length == 10){
            var reg = /(0[1-9]|[12][0-9]|3[01])[- /.](0[1-9]|1[012])[- /.](19|20)\d\d/;
            if (data.match(reg))
                return true;
        }
        return false;
    }

    function listen() {
        //se preme Invio
        if(event.keyCode == 13)
            cerca();
    }

    $( document ).ready(function() {

        params = {
            format: "dd/mm/yyyy",
            autoclose:true,
            locale: 'it'
        };
        $('#data_inizio').datepicker(params);
        $('#data_fine').datepicker(params);


    });
</script>
@endsection