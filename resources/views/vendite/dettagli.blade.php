@extends('layouts.app')
@section('content')
    <div class="container">
        <div class="row">
            <div id="placeholder"></div>
            <div class="col-sm-12">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h2>Vendita del {{date('d F Y', strtotime($vendita->created_at))}} </h2>
                        <div class="btn-group pull-right">
                            <span onclick="conferma('{{$vendita->id}}', '/vendite/')" class="btn btn-danger btn-md">elimina</span>
                        </div>
                        effettuato da <b>{{$vendita->user->nome}}</b><br>
                        il {{date('d F Y', strtotime($vendita->created_at))}} alle {{date('H:i', strtotime($vendita->created_at))}}

                    </div>
                    <div class="panel-body">
                        <table class="table table-hover">
                            <thead>
                            <tr>
                                <th>Nome</th>
                                <th>Descrizione</th>
                                <th>Quantita</th>
                                <th>Prezzo applicato</th>
                                <th>Prezzo di vendita</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($oggetti as $oggetto)
                                <tr style="cursor: pointer" onclick="window.location='{{ url("oggetti/".$oggetto->id."") }}'">
                                    <td>{{$oggetto->nome}}</td>
                                    <td>{{$oggetto->descrizione}}</td>
                                    <td>{{$oggetto->pivot->quantita}}</td>
                                    <td>{{$oggetto->pivot->prezzo_applicato}}</td>
                                    <td>{{$oggetto->prezzo_vendita}}</td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @include('common.elimina_modal')
@endsection