@extends('layouts.app')
@section('content')
<div class="container">
    <link rel="stylesheet" href="/css/datepicker.css">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-success">
                <div class="panel-heading"><h3 class="panel-title">Nuova vendita</h3></div>
                <div class="panel-body">
                    <div class="row" id="form_vendita">
                        <div class="col-md-9">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="col-md-12">
                                        <input onkeyup="cerca()" autofocus="autofocus" id="search" type="text" class="form-control" placeholder="Cerca un oggetto..">
                                    </div>
                                    <div class="col-md-12 col-lg-3 col-lg-offset-9">
                                        <input id="data" type="text" class="form-control" placeholder="Data">
                                    </div>
                                </div>
                            </div>

                            <br>
                            <div id="risultati" class="row">
                                @include('oggetti.risultati_ricerca_vendita')
                            </div>
                        </div>

                        <div class="col-md-3">
                            <div class="panel panel-primary">
                                <div class="panel-heading">In vendita</div>
                                <table class="table">
                                    <thead>
                                        <tr><th>Nome</th><th>Prezzo</th><th>Quantita</th><th></th></tr>
                                    </thead>
                                    <tbody id="body_aggiunti">
                                    </tbody>
                                </table>

                                <div class="panel-body">
                                    <div id="alertVuoto" class="center-block">
                                        <h5><span class="label label-info">Lista vuota</span></h5></div>
                                </div>
                            </div>

                            <button onclick="confermaVendita()" class="btn btn-success btn-lg pull-right">
                                <i class="fa fa-btn"></i> Conferma
                            </button>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


<script src="/js/datepicker.js"></script>
<script type="text/javascript">

    var lista_oggetti = [];
    var lista_quantita = [];
    var lista_prezzi = [];

    function aggiungi(id, nome, max){
        if(lista_oggetti.indexOf(""+id) == -1) {
            var prezzo_applicato = $('#'+id).find('#prezzo_applicato').val();
            var quantita = $('#'+id).find('#quantita').val();

            if(parseInt(quantita) > parseInt(max)){
                alert('Errore! La quantita venduta supera la disponibilità dell\'oggetto(' + max + ')');
            }else {
                lista_oggetti.push("" + id);
                lista_quantita.push(quantita);
                lista_prezzi.push(prezzo_applicato);

                var tr = '<tr style="cursor: pointer">' +
                        '<td>' + nome + '</td>' +
                        '<td>' + prezzo_applicato + '</td>' +
                        '<td>' + quantita + '</td>' +
                        '<td><button onclick ="togli($(this),' + id + ')" type="button" class="btn btn-sm btn-danger">X</button></td>' +
                        '</tr>;'
                $("#body_aggiunti").append(tr);
            }
        }else{
            alert('oggetto gia presente');
        }

        if(lista_oggetti.length == 1)
            $("#alertVuoto").hide();
    }

    function togli(e, id){
        e.closest('tr').remove();

        var index = lista_oggetti.indexOf(""+id);
        lista_oggetti.splice(index, 1 );
        lista_quantita.splice(index, 1 );
        lista_prezzi.splice(index, 1 );

        if(lista_oggetti.length == 0)
            $("#alertVuoto").show();
    }

    function confermaVendita(){
        if(lista_oggetti.length > 0) {
            data_vendita = $('#data').val();
            post_url = '/vendite/nuova';

            $.ajax({
                type: "POST",
                url: post_url,
                data: {
                    _token: '{{ csrf_token() }}',
                    oggetti : lista_oggetti,
                    quantita : lista_quantita,
                    prezzi : lista_prezzi,
                    data_vendita : data_vendita
                },
                success: function( data ){
                    $('#form_vendita').html(data);
                },
            });

        }else{
            $("#risultati").html(
                    '<div class="alert alert-dismissible alert-danger"><strong>Errore!</strong> <a href="#" class="alert-link">Lista oggetti vuota!</div>'
            );
        }
    }

    function cerca()
    {
        var query_string = $("#search").val();
        var query_url = '/oggetti/cerca/'+query_string;

        $.ajax({
            url: query_url,
            type: 'GET',
            data : {
                src: 'vendita'
            },
            success: function( data ){
                $("#risultati").html(data.tabella);
            },
            error: function (xhr, b, c) {
                console.log("errore in ajax query");
            }
        });
    }

    $( document ).ready(function() {
        params = {
            format: "dd/mm/yyyy",
            autoclose:true,
            locale: 'it'
        };
        $('#data').datepicker(params);
    });
</script>


@if(isset($_GET['q'])){
<script type="text/javascript">
    $(document).ready(function(){
        q = "{{$_GET['q']}}";
        $("#search").val(q);
        cerca();
    });
</script>
}
@endif

@endsection