<div class="col-md-offset-1 col-md-10">
    <h3>Vendita effettuata correttamente.</h3>
    <h4>
        <a href="/vendite"class="btn btn-default">Homepage vendite</a>
        <a href="/vendite/nuova"class="btn btn-success">Nuova vendita</a>
    </h4>
    <div class="panel panel-primary">
        <div class="panel-heading">Dettagli</div>
        <div class="panel-body">
            <table class="table">
                <thead>
                    <tr><th>Nome</th><th>Prezzo</th><th>Quantita</th><th></th></tr>
                </thead>
                <tbody id="body_aggiunti">
                    @for($i = 0; $i < count($oggetti); $i++)
                        <tr style="cursor: pointer">
                            <td>{{$oggetti[$i]}}</td>
                            <td>{{$prezzi[$i]}}</td>
                            <td>{{$quantita[$i]}}</td>
                       </tr>
                    @endfor
                </tbody>
            </table>
        </div>
    </div>
</div>