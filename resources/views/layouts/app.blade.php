<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>WOW Spazio Fumetto</title>

    <!-- Fonts -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css" integrity="sha384-XdYbMnZ/QjLh6iI4ogqCTaIjrFk87ip+ekIjefZch0Y+PvJ8CDYtEs1ipDmPorQ+" crossorigin="anonymous">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Lato:100,300,400,700">

    <!-- Bootstrap style: paper theme -->
    <link rel="stylesheet" href="{{ asset('bootstrap/css/bootstrap.min.css') }}">

    <!-- JQuery e js di bootstrap -->
    <script src="/js/jquery-3.1.1.min.js"></script>
    <script src="/bootstrap/js/bootstrap.min.js"></script>
    {{-- <script src="{{ elixir('js/app.js') }}"></script> --}}


    <style>
        body {
            background: url("/images/books.jpg")  no-repeat center center fixed;
            -webkit-background-size: cover;
            -moz-background-size: cover;
            -o-background-size: cover;
            background-size: cover;
        }

        .fa-btn {
            margin-right: 6px;
        }
        .dropdown:hover .dropdown-menu {
            display: block;
        }
    </style>
</head>
<body id="app-layout">
    <nav class="navbar navbar-inverse navbar-static-top">
        <div class="container">
            <div class="navbar-header">

                <!-- Collapsed Hamburger -->
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#app-navbar-collapse">
                    <span class="sr-only">Toggle Navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>

                <!-- Branding Image -->
                @if(Auth::user()->tipologia == 'admin')
                    <a class="navbar-brand" href="/admin">
                        <img align="left" alt="WOW" src="http://www.museowow.it/wow/wp-content/uploads/2014/12/logo_wow.png" style="width:auto; height:100%; ">
                        Admin
                    </a>
                @elseif(Auth::user()->tipologia == 'magazzino')
                    <a class="navbar-brand" href="/">
                        <img align="left" alt="WOW" src="http://www.museowow.it/wow/wp-content/uploads/2014/12/logo_wow.png" style="width:auto; height:100%; ">
                        Magazzino
                    </a>
                @else
                    <a class="navbar-brand" href="/vendite">
                        <img align="left" alt="WOW" src="http://www.museowow.it/wow/wp-content/uploads/2014/12/logo_wow.png" style="width:auto; height:100%; ">
                        Vendite
                    </a>
                @endif
            </div>

            <div class="collapse navbar-collapse" id="app-navbar-collapse">
                <!-- Left Side Of Navbar -->
                <ul class="nav navbar-nav">
                @if(Auth::user()->tipologia == 'admin')
                    <li id="home" ><a href="{{ url('/admin') }}">Utenti</a></li>
                    <li id="vendite" ><a href="{{ url('/admin/registra') }}">Nuovo Utente</a></li>
                @elseif(Auth::user()->tipologia == 'magazzino')
                    <li id="home" ><a href="{{ url('/home') }}">Home</a></li>
                    <li id="vendite" ><a href="{{ url('/vendite') }}">Vendite</a></li>
                    <li id="ordini" ><a href="{{ url('/ordini') }}">Ordini</a></li>
                    <li id="oggetti" ><a href="{{ url('/oggetti') }}">Oggetti</a></li>
                    <li id="fornitori" ><a href="{{ url('/fornitori') }}">Fornitori</a></li>
                @else
                    <li id="home" ><a href="{{ url('/home') }}">Home</a></li>
                    <li id="vendite" ><a href="{{ url('/vendite') }}">Vendite</a></li>
                    <li id="oggetti" ><a href="{{ url('/oggetti') }}">Oggetti</a></li>
                @endif
                </ul>
                <!-- Right Side Of Navbar -->
                <ul class="nav navbar-nav navbar-right">
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                            {{ Auth::user()->nome }} {{ Auth::user()->cognome }}<span class="caret"></span>
                        </a>

                        <ul class="dropdown-menu" role="menu">
                            <li><a href="/utenti/{{Auth::user()->id}}"><i class="fa fa-btn glyphicon glyphicon-user"></i>Profilo</a></li>
                            <li><a href="/utenti/modifica/{{Auth::user()->id}}"><i class="fa glyphicon glyphicon-wrench"></i>Aggiorna dati</a></li>
                            <li><a href="{{ url('/logout') }}"><i class="fa fa-btn fa-sign-out"></i>Esci</a></li>
                        </ul>
                    </li>
                </ul>
            </div>
        </div>
    </nav>

    @yield('content')

    <!-- set active nav item -->
    <script>
        $( document ).ready(function() {
            var active = $(location).attr('pathname');
            if(active == "")
                $("#home").addClass('active');

            active.indexOf(1);
            active.toLowerCase();
            active = active.split("/")[1];

            $("#"+active).addClass('active');
        });
    </script>
</body>
</html>
