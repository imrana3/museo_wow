@extends('layouts.app')
@section('content')
<div class="container">
    <div class="panel panel-default">
        <div class="panel-heading">
            <h4>Gestisci fornitori di {{$nome}}</h4>
            <h6>Seleziona dalla lista i possibili fornitori di questo oggetto.</h6>
        </div>
        <div class="panel-body">
        @if(count($fornitori_oggetto) + count($fornitori) > 0)
        <form class="form-horizontal" role="form" method="POST" action="/oggetti/fornitori/{{$id}}">
            {{ csrf_field() }}
            <input type="hidden" value="ciao"/>
            <table class="table-hover table" style="cursor: pointer">
                <thead>
                    <tr><th>Seleziona</th><th>Nome</th><th>Indirizzo</th></tr>
                </thead>
                <tbody>
                @foreach($fornitori_oggetto as $fornitore)
                    <tr>
                        <td><input type="checkbox" name="fornitori[]" checked="true" value="{{$fornitore->id}}"></td>
                        <td><b>{{$fornitore->nome}}</b></td>
                        <td>{{$fornitore->indirizzo}}</td>
                    </tr>
                @endforeach
                @foreach($fornitori as $fornitore)
                    <tr>

                        <td><input type="checkbox" name="fornitori[]" value="{{$fornitore->id}}"/></td>
                        <td>{{$fornitore->nome}}</td>
                        <td>{{$fornitore->indirizzo}}</td>
                    </tr>
                @endforeach
                </tbody>
            </table>

            <div class="form-group">
                <div class="pull-right">
                    <span  onclick="window.location='/oggetti/{{$id}}'" class="btn btn-danger">Annulla</span>
                    <button type="submit" class="btn btn-success">Aggiorna</button>
                </div>
            </div>
        </form>
        @else
            <p>Nessun fornitori presente nel sistema.</p>
        @endif
        </div>
    </div>
</div>
<script type="text/javascript">
    $("td").click(function(e) {
        var chk = $(this).closest("tr").find("input:checkbox").get(0);
        if(e.target != chk)
        {
            chk.checked = !chk.checked;
        }
    });
</script>
@endsection
