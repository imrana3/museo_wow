@extends('layouts.app')
@section('content')
<div class="container">
    <div id="placeholder"></div>
    <div class="row">
        <div class="col-md-9">
            @yield('dettagli_oggetto')
        </div>
        <div class="col-md-3">
            @include('oggetti.tabella_fornitori')
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            @include('oggetti.storico_oggetto')
        </div>
    </div>
    @include('common.elimina_modal')
</div>
@endsection