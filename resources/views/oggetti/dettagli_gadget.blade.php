@extends('oggetti.dettagli')
@section('dettagli_oggetto')
    <div class="panel panel-default">
        <div class="panel-heading">
            <h1 style="padding-top: 7.5px;">
                {{$oggetto->nome}}
            </h1>
            <div class="btn-group pull-right">
                <a href="/ordini/nuovo/?q={{$oggetto->nome}}" class="btn btn-success btn-md">ordina</a>
                <a href="/oggetti/modifica/{{$oggetto->id}}" class="btn btn-warning btn-md">modifica</a>
                <span onclick="conferma('{{$oggetto->id}}', '/oggetti/')"  class="btn btn-danger btn-md">elimina</span>
            </div>
            <h5>
                {{date('d F Y', strtotime($oggetto->created_at))}}<br>
                alle {{date('H:i', strtotime($oggetto->created_at))}}
            </h5>
        </div>
        <div class="panel-body">
            <div class="col-lg-6 col-md-6">
                <h5>
                    <span class="glyphicon glyphicon-wrench"></span>
                    <b>Produttore</b>
                    @if($oggetto->produttore != "")
                        {{$oggetto->produttore}}<br>
                    @else
                        <i>non definito</i><br>
                    @endif
                    <span class="glyphicon glyphicon-home"></span>
                    <b>Luogo</b>
                    @if($oggetto->luogo_giacenza != "")
                        {{$oggetto->luogo_giacenza}}<br>
                    @else
                        <i>non definito</i><br>
                    @endif
                    <span class="glyphicon glyphicon-sound-5-1"></span> <b>Quantita</b> {{$oggetto->quantita}}<br>
                </h5>
            </div>
            <div class="col-lg-6 col-md-6">
                <h5>
                    <span class="glyphicon glyphicon-euro"></span> <b>Prezzo d'acquisto</b> {{$oggetto->prezzo_acquisto}}<br>
                    <span class="glyphicon glyphicon-euro"></span> <b>Prezzo vendita</b> {{$oggetto->prezzo_vendita}}<br>
                    <span class="glyphicon glyphicon-plus"></span>
                    <b>IVA</b>
                    @if($oggetto->iva != "")
                        {{$oggetto->iva}}<br>
                    @else
                        <i>non definita</i><br>
                    @endif
                </h5>
            </div><hr>
            <div class="col-lg-8 col-md-8">
                @if($oggetto->descrizione != "")
                    <h5>{{$oggetto->descrizione}}</h5>
                @else
                    <h5><i>Nessuna descrizione presente. Modifica l'oggetto per aggiungerla.</i></h5>
                @endif
            </div>
        </div>
    </div>
@endsection

