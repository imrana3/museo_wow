@if(count($libri) > 0)
    <table id="libri_table" class="table table-hover">
        <thead>
        <tr>

            <th>Azioni</th>
            <th>Barcode</th>
            <th>Titolo</th>
            <th>Autore</th>
            <th>Descrizione</th>
            <th>Disponibilità</th>
        </tr>
        </thead>
        <tbody>
        @foreach($libri as $libro)
            <tr style="cursor: pointer">
                <td>
                    <a href="/ordini/nuovo/?q={{$libro->nome}}" class="btn btn-success btn-sm glyphicon glyphicon-plus"></a>
                    <a href="/vendite/nuova?q={{$libro->nome}}" class="btn btn-info btn-sm glyphicon glyphicon-euro"></a>
                    <a href="/oggetti/modifica/{{$libro->id}}" class="btn btn-warning btn-sm glyphicon glyphicon-pencil"></a>
                    <span onclick="conferma('{{$libro->id}}', '/oggetti/')" class="btn btn-danger btn-sm glyphicon glyphicon-trash"></span>
                </td>
                <td onclick=" window.location='oggetti/{{$libro->id}}'" >{{$libro->barcode}}</td>
                <td onclick=" window.location='oggetti/{{$libro->id}}'" >{{$libro->nome}}</td>
                <td onclick=" window.location='oggetti/{{$libro->id}}'" >{{$libro->libro->autore}}</td>
                <td onclick=" window.location='oggetti/{{$libro->id}}'" >{{str_limit($libro->descrizione, 150)}}</td>
                <td onclick=" window.location='/oggetti/{{$libro->id}}'" >{{$libro->quantita}}</td>
            </tr>
        @endforeach
        </tbody>
    </table>
    <div class="pagination"> {{ $libri->links() }} </div>
@else
    <div><h4>Nessun libro presente</h4></div>
@endif