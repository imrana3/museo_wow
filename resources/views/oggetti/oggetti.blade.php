@extends('layouts.app')
@section('content')
<div class="container">

    <div id="placeholder"></div>
    <p><button onclick="window.location='oggetti/nuovo'"  type="button" class="btn btn-success btn-lg">Nuovo oggetto</button></p>
    <div class="panel panel-default">
        <div class="panel-body">
            <ul class="nav nav-tabs" id="tabs">
                <li class="active" id="tab_libri">
                    <a data-toggle="tab" href="#libri">Libro</a>
                </li>
                <li id="tab_gadget">
                    <a data-toggle="tab" href="#gadget">Gadget</a>
                </li>
            </ul>

            <div class="tab-content">
                <div id="libri" class="tab-pane fade in active col-lg-12 centered">
                    <div class="input-group col-lg-6">
                        <br><input onkeyup="listen('libri')" type="text"  id="search_libri"  class="form-control" placeholder="Cerca libro..." autofocus>
                        <span class="input-group-btn">
                            <a onclick="cerca('libri')" href="#" class="btn btn-success btn-md">Cerca</a>
                            <span id="annulla_libri"></span>
                        </span>
                    </div>
                    <div id="tabella_libri">
                        @include('oggetti.tabella_libri')
                    </div>
                </div>

                <div id="gadget" class="tab-pane fade col-lg-12 centered">
                    <div class="input-group col-lg-6">
                        <br><input onkeyup="listen('gadget')" type="text"  id="search_gadget"  class="form-control" placeholder="Cerca gadget..." autofocus>
                        <span class="input-group-btn">
                                <a onclick="cerca('gadget')"  class="btn btn-success btn-md">Cerca</a>
                                <span id="annulla_gadget"></span>
                            </span>
                    </div>
                    <div id="tabella_gadget">
                        @include('oggetti.tabella_oggetti')
                    </div>
                </div>

            </div>
        </div>
    </div>

    <link href="https://cdn.datatables.net/plug-ins/1.10.7/integration/bootstrap/3/dataTables.bootstrap.css" rel="stylesheet" />
    <script src="https://cdn.datatables.net/1.10.7/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/plug-ins/1.10.7/integration/bootstrap/3/dataTables.bootstrap.js"></script>

    @include('common.elimina_modal')

    @if(isset($_GET['gadget']))
        <script type="text/javascript">
            $('.nav-tabs a[href="#gadget"]').tab('show');
        </script>
    @endif

    <!-- script per datatables (ordinamento, ricerca) -->
    <script>
        var datatable_params = {
            "language": {
                "url": "//cdn.datatables.net/plug-ins/1.10.12/i18n/Italian.json"
            },
            "sDom":"<'row'<'col-sm-12'tr>>" +
            "<'row'<'col-sm-5'><'col-sm-7'>>"
        };

        var gadget_table = $('#gadget_table').DataTable(datatable_params);
        var libri_table = $('#libri_table').DataTable(datatable_params);

    </script>

    <script type="text/javascript">
        /*==================== PAGINATON AJAX =========================*/

        function listen(src) {
            //se preme Invio
            if(event.keyCode == 13)
                cerca(src);
        }

        function cerca(src)
        {
            $('#annulla_'+src).html(' <a href="/oggetti" class="btn btn-danger btn-md">Annulla ricerca</a>')
            getResults(0,src);
        }

        function getResults(page, src){

            var query_string = $("#search_"+src).val();
            var query_url = '/oggetti/cerca/'+query_string;
            $.ajax({
                url: query_url,
                type: 'GET',
                data : {
                    src: src,
                    page: page
                },
                success: function( data ){
                    $("#tabella_"+src).html(data.tabella);
                }
            });
        }

        $(window).on('hashchange',function(){
            page = window.location.hash.replace('#','');
            if($("ul#tabs li.active").attr('id') == "tab_gadget")
                getResults(page, 'gadget');
            else
                getResults(page,'libri');

        });

        $(document).on('click','.pagination a', function(e){
            if($('#annulla_gadget').text().length > 0 || $('#annulla_libri').text().length > 0) {
                e.preventDefault();
                var page = $(this).attr('href').split('page=')[1];
                // getResults(page);
                location.hash = page;
            }
        });
    </script>

    <!-- set autofocus on filtra_x input -->
    <script type="text/javascript">
        $('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
            var target = e.target.attributes.href.value;
            $(target +' input').focus();
        })
    </script>
</div>
@endsection