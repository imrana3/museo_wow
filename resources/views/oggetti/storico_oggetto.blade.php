<div class="row">
    <div class="col-sm-6">
        <div class="panel panel-default">
            <div class="panel-heading"><h4>Storico Ordini</h4></div>
            <div class="panel-body">
                @if(count($ordini) == 0 )
                <div>
                    L'oggetto non è prensente in alcun ordine.<br>
                    <a href="/ordini/nuovo/?q={{$oggetto->nome}}" class="btn btn-success btn-md">ordina</a>
                </div>
                 @else
                    @foreach($ordini as $ordini_mese)
                        <div class="panel panel-default">
                            <div class="panel-body">
                                <h5>
                                    <b>{{date('F Y', strtotime($ordini_mese[0]->data))}}</b>
                                    <span class="pull-right">{{count($ordini_mese)}} ordini</span>
                                </h5>
                                <table class="table table-hover">
                                    <thead>
                                    <tr>
                                        <th>Data</th>
                                        <th>Nome</th>
                                        <th>Quantità</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($ordini_mese as $ordine)
                                        <tr onclick="window.location = '/ordini/{{$ordine->id}}'" style="cursor:pointer;">
                                            <td>
                                                {{date('d F Y', strtotime($ordine->created_at))}}<br>
                                                alle {{date('H:i', strtotime($ordine->created_at))}}
                                            </td>
                                            <td>{{$ordine->nome}}</td>
                                            <td>{{$ordine->pivot['quantita']}}</td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    @endforeach
                @endif
            </div>
        </div>
    </div>
    <div class="col-sm-6">
        <div class="panel panel-default">
            <div class="panel-heading"><h4>Storico vendite</h4></div>
            <div class="panel-body">
        @if(count($vendite) == 0 )
            <div>
                Storico vendite vuoto.<br>
                <a href="/vendite/nuova/?q={{$oggetto->nome}}" class="btn btn-success btn-md">vendi</a>
            </div>
        @else
            @foreach($vendite as $vendite_mese)
                <div class="panel panel-default">
                    <div class="panel-body">
                        <h5>
                            <b>{{date('F Y', strtotime($vendite_mese[0]->data))}}</b>
                            <span class="pull-right">{{count($vendite_mese)}} vendite</span>
                        </h5>
                        <table class="table table-hover">
                            <thead>
                            <tr>
                                <th>Data</th>
                                <th>Quantità</th>
                                <th>Prezzo</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($vendite_mese as $vendita)
                            <tr onclick="window.location = '/vendite/{{$vendita->id}}'" style="cursor:pointer;">
                                <td>
                                    {{date('d F Y', strtotime($vendita->data))}}<br>
                                    alle {{date('H:i', strtotime($vendita->data))}}
                                </td>
                                <td>{{$vendita->pivot['quantita']}}</td>
                                <td>{{$vendita->pivot['prezzo_applicato']}}</td>
                            </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            @endforeach
        @endif
                </div>
            </div>
    </div>
</div>
