@if($oggetti_trovati->count() == 0)
    <div class="col-md-12">
        <h2>nessun oggetto trovato</h2>
        <a href="/oggetti/nuovo" target="_blank"><button type="button" class="btn btn-success btn-lg" data-toggle="modal">
                inserisci ora
            </button></a>
    </div>
@else
    <div class="col-md-12">
        <h3>Risultati ricerca</h3>
        <table class="table table-hover table-responsive">
            <thead>
            <tr>
                <th>Nome</th>
                <th>Prezzo</th>
                <th>Quantita</th>
                <th></th>
            </tr>
            </thead>
            <tbody>
            @foreach($oggetti_trovati as $oggetto)
                <tr id="{{$oggetto->id}}">
                    <td>{{$oggetto->nome}}</td>
                    <td><input type="number" class="form-control" id="prezzo_applicato" value="{{$oggetto->prezzo_vendita}}"/></td>
                    <td><input type="number" min="1" step="1" class="form-control" id="quantita" value="1"/> disponibili: {{$oggetto->quantita}}</td>
                    <td><button onclick="aggiungi('{{ $oggetto->id}}', '<? echo addslashes($oggetto->nome) ?>', '{{$oggetto->quantita}}')" type="button" class="btn btn-sm btn-success">+</button></td>
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>
@endif