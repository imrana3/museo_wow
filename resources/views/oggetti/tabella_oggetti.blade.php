@if(count($oggetti) > 0)
    <table id="gadget_table" class="table table-hover ">
        <thead>
        <tr>
            <th>Azioni</th>
            <th>Nome</th>
            <th>Descrizione</th>
            <th>Produttore</th>
            <th>Disponibilità</th>
        </tr>
        </thead>
        <tbody>
        @foreach($oggetti as $oggetto)
            <tr style="cursor: pointer">
                <td>
                    <a href="/ordini/nuovo/?q={{$oggetto->nome}}" class="btn btn-success btn-sm glyphicon glyphicon-plus"></a>
                    <a href="/vendite/nuova?q={{$oggetto->nome}}" class="btn btn-info btn-sm glyphicon glyphicon-euro"></a>
                    <a href="/oggetti/modifica/{{$oggetto->id}}" class="btn btn-warning btn-sm glyphicon glyphicon-pencil"></a>
                    <span onclick="conferma('{{$oggetto->id}}', '/oggetti/')"   class="btn btn-danger btn-sm glyphicon glyphicon-trash"></span>
                </td>
                <td onclick=" window.location='/oggetti/{{$oggetto->id}}'" >{{$oggetto->nome}}</td>
                <td onclick=" window.location='/oggetti/{{$oggetto->id}}'" >{{str_limit($oggetto->descrizione, 150)}}</td>
                <td onclick=" window.location='/oggetti/{{$oggetto->id}}'" >{{$oggetto->produttore}}</td>
                <td onclick=" window.location='/oggetti/{{$oggetto->id}}'" >{{$oggetto->quantita}}</td>
            </tr>
        @endforeach
        </tbody>
    </table>
    <div class="pagination"> {{ $oggetti->links() }} </div>
@else
    <div><h4>Nessun gadget presente</h4></div>
@endif