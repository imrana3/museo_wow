
<div class="form-group{{ $errors->has('nome') ? ' has-error' : '' }}">
    <label for="nome" class="col-md-4 control-label">Nome</label>
    <div class="col-md-6">
        <input id="nome" type="text" class="form-control" autofocus="autofocus"  name="nome" value="{{ isset($oggetto) ? $oggetto['nome'] : old('nome') }}">
        @if ($errors->has('nome'))
            <span class="help-block">
                <strong>{{ $errors->first('nome') }}</strong>
            </span>
        @endif
    </div>
</div>

<div class="form-group{{ $errors->has('produttore') ? ' has-error' : '' }}">
    <label for="produttore" class="col-md-4 control-label">Produttore</label>
    <div class="col-md-6">
        <input id="produttore" type="text" class="form-control" name="produttore" value="{{ isset($oggetto) ? $oggetto['produttore'] : old('produttore') }}">
        @if ($errors->has('produttore'))
            <span class="help-block">
                <strong>{{ $errors->first('produttore') }}</strong>
            </span>
        @endif
    </div>
</div>

<div class="form-group{{ $errors->has('descrizione') ? ' has-error' : '' }}">
    <label for="descrizione" class="col-md-4 control-label">Descrizione</label>
    <div class="col-md-6">
        <textarea id="descrizione" class="form-control" rows="5" name="descrizione">{{ isset($oggetto) ? $oggetto['descrizione'] :old('descrizione') }}</textarea>
        @if ($errors->has('descrizione'))
            <span class="help-block">
                <strong>{{ $errors->first('descrizione') }}</strong>
            </span>
        @endif
    </div>
</div>

<div class="form-group{{ $errors->has('quantita') ? ' has-error' : '' }}">
    <label for="quantita" class="col-md-4 control-label">Quantità</label>
    <div class="col-md-6">
        <input id="quantita" type="text" class="form-control" name="quantita" value="{{ isset($oggetto) ? $oggetto['quantita'] : old('quantita') }}">
        @if ($errors->has('quantita'))
            <span class="help-block">
                <strong>{{ $errors->first('quantita') }}</strong>
            </span>
        @endif
    </div>
</div>

<div class="form-group{{ $errors->has('luogo_giacenza') ? ' has-error' : '' }}">
    <label for="luogo_giacenza" class="col-md-4 control-label">Luogo giacenza</label>
    <div class="col-md-6">
        <input id="luogo_giacenza" type="text" class="form-control" name="luogo_giacenza" value="{{ isset($oggetto) ? $oggetto['luogo_giacenza'] :old('luogo_giacenza') }}">
        @if ($errors->has('luogo_giacenza'))
            <span class="help-block">
                <strong>{{ $errors->first('luogo_giacenza') }}</strong>
            </span>
        @endif
    </div>
</div>

<div class="form-group{{ $errors->has('prezzo_vendita') ? ' has-error' : '' }}">
    <label for="prezzo_vendita" class="col-md-4 control-label">Prezzo di acquisto</label>
    <div class="col-md-6">
        <input id="prezzo_vendita" type="text" class="form-control" name="prezzo_acquisto" value="{{ isset($oggetto) ? $oggetto['prezzo_acquisto'] : old('prezzo_acquisto') }}">
        @if ($errors->has('prezzo_vendita'))
            <span class="help-block">
                <strong>{{ $errors->first('prezzo_vendita') }}</strong>
            </span>
        @endif
    </div>
</div>

<div class="form-group{{ $errors->has('prezzo_acquisto') ? ' has-error' : '' }}">
    <label for="prezzo_vendita" class="col-md-4 control-label">Prezzo di vendita</label>
    <div class="col-md-6">
        <input id="prezzo_vendita" type="text" class="form-control" name="prezzo_vendita" value="{{ isset($oggetto) ? $oggetto['prezzo_vendita'] : old('prezzo_vendita') }}">
        @if ($errors->has('prezzo_acquisto'))
            <span class="help-block">
                <strong>{{ $errors->first('prezzo_acquisto') }}</strong>
            </span>
        @endif
    </div>
</div>

<div class="form-group{{ $errors->has('iva') ? ' has-error' : '' }}">
    <label for="iva" class="col-md-4 control-label">Iva </label>
    <div class="col-md-6">
        <input id="iva" type="text" class="form-control" name="iva" value="{{ isset($oggetto) ? $oggetto['iva'] :old('iva') }}">
        @if ($errors->has('iva'))
            <span class="help-block">
                <strong>{{ $errors->first('iva') }}</strong>
            </span>
        @endif
    </div>
</div>

@if(isset($fornitori))
<div class="form-group">
    <label for="fornitori" class="col-md-4 control-label">Fornitori<br>Seleziona tutti i possibili fornitori di questo oggetto</label>
    <div class="col-md-6">
        @foreach($fornitori as $fornitore)
            <span class="col-lg-4 col-sm-4  col-md-6 col-xs-6">
            <input type="checkbox" name="fornitori[]" value="{{$fornitore->id}}"/>{{$fornitore->nome}}
            </span>
        @endforeach
    </div>
</div>
@endif

