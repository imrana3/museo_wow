@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-10 col-md-offset-1">
                @if(Session::has('success'))
                    <div class="alert alert-success">
                        <div class="container">
                            <h5><strong>Fatto! </strong>{{Session::get('success')}}</h5>
                            <a href="/oggetti" class="btn btn-default">Homepage oggetti</a>
                            <a href="/oggetti/{{Session::get('id')}}" class="btn btn-primary">Visualizza gadget</a>
                            <a href="/oggetti/nuovo" class="btn btn-success">Inserisci nuovo</a>
                        </div>
                    </div>
                @endif

                <div class="panel panel-default">
                    <div class="panel-heading">Inserisci nuovo oggetto</div>
                    <div class="panel-body">
                        <ul class="nav nav-tabs">
                            <li class="active"><a data-toggle="tab" href="#libro">Libro</a></li>
                            <li><a data-toggle="tab" href="#gadget">Gadget</a></li>
                        </ul>
                        <div class="tab-content">
                            <div id="libro" class="tab-pane fade in active col-lg-12 col-offset-6 centered">
                                <form class="form-horizontal" role="form" method="POST" action="{{ url('/oggetti/libro') }}">
                                    {{ csrf_field() }}
                                    @include('oggetti.libro_form')
                                    <div class="form-group">
                                        <div class="col-md-6 col-md-offset-4">
                                            <button type="submit" class="btn btn-primary">
                                                <i class="fa fa-btn"></i> Inserisci
                                            </button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                            <div id="gadget" class="tab-pane fade">
                                <form class="form-horizontal" role="form" method="POST" action="{{ url('/oggetti/gadget') }}">
                                    {{ csrf_field() }}
                                    @include('oggetti.gadget_form')
                                    <div class="form-group">
                                        <div class="col-md-6 col-md-offset-4">
                                            <button type="submit" class="btn btn-primary">
                                                <i class="fa fa-btn"></i> Inserisci
                                            </button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <script type="text/javascript">
            $('#barcode').on('input', function() {
                cerca();
            });

            function cerca() {
                isbn = $("#barcode").val();

                var query_url = "https://www.googleapis.com/books/v1/volumes?q=isbn:" + isbn +"&fields=items(volumeInfo)";
                $.ajax({
                    type: 'GET',
                    url: query_url,
                    success: function (data) {
                        items = data.items;
                        if(data.items){
                            console.log(items[0]["volumeInfo"]);

                            var book = data.items[0];
                            var titolo =book["volumeInfo"]["title"];
                            var autore = book["volumeInfo"]["authors"][0];
                            var editore = book["volumeInfo"]["publisher"];
                            if(book["volumeInfo"]["description"])
                                var desc = book["volumeInfo"]["description"];

                            console.log(book);
                            $('#nome').val(titolo);
                            $('#autore').val(autore);
                            $('#editore').val(editore);
                            $('#descrizione').val(desc);

                        }else{

                            $("#risultati").html("<h3>non trovato<h3>");
                        }
                    }
                });
            }
        </script>
    </div>
@endsection
