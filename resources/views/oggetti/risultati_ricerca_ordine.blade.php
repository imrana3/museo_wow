@if($oggetti_trovati->count() == 0)
    <div class="col-md-12">
        <h2>nessun oggetto trovato</h2>
        <a href="/oggetti/nuovo" target="_blank"><button type="button" class="btn btn-success btn-lg" data-toggle="modal">
            inserisci ora
        </button></a>
    </div>
@else
<div class="col-md-12">
    <table class="table table-hover table-responsive">
        <thead>
        <tr>
            <th>Nome</th>
            <th>Fornitore</th>
            <th>Quantita</th>
            <th></th>
        </tr>
        </thead>
        <tbody>
        @for($i=0; $i < count($oggetti_trovati); $i++)
            <tr id="{{$oggetti_trovati[$i]->id}}">
                <td>{{$oggetti_trovati[$i]->nome}}</td>
                <td>
                    <div class="form-group">
                        <select class="form-control" id="fornitore">
                        @foreach($fornitori[$i] as $fornitore)
                            <option value="{{$fornitore->id}}">{{$fornitore->nome}}</option>
                        @endforeach
                        </select>
                    </div>
                </td>
                <td><input type="number" min="1" step="1" class="form-control" id="quantita" value="1"/> disponibili: {{$oggetti_trovati[$i]->quantita}}</td>
                <td><button onclick="aggiungi('{{ $oggetti_trovati[$i]->id}}', '<? echo addslashes($oggetti_trovati[$i]->nome) ?>')" type="button" class="btn btn-sm btn-success">+</button></td>
            </tr>
        @endfor
        </tbody>
    </table>
</div>
@endif