{{--Ultimi x oggetti registrati--}}
<div class="col-md-6 col-sm-6 col-lg-6"  style="cursor: pointer;">
    <div class="panel panel-default">
        <div class="panel-heading">
            <div class="btn-group pull-right">
                <a href="/oggetti/" class=" btn btn-sm btn-primary gl-plus">visualizza tutti</a>
                <a href="/oggetti/nuovo" class="btn btn-sm btn-success gl-plus">nuovo</a>
            </div>
            <h5>Nuovi oggetti</h5>
        </div>
        <div class="panel-body">
            <table class="table table-hover">
                <thead><th>Data</th><th>Nome</th><th>Quantita</th></thead>
                <tbody>
                @foreach($oggetti as $oggetto)
                    <tr onclick="window:location='/oggetti/{{$oggetto->id}}'" data-toggle="tooltip" data-delay='{"show":"3000"}' title="clicca per visualizzare i dettagli dell'oggetto">
                        <td>
                            {{date('d F Y', strtotime($oggetto->created_at))}}<br>
                            alle {{date('H:i', strtotime($oggetto->created_at))}}
                        </td>
                        <td><b>{{$oggetto->nome}}</b></td>
                        <td>{{$oggetto->quantita}}</td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>