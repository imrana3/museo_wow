@extends('layouts.app')
@section('content')
    <div class="container">
        <div class="row">
            @if(Session::has('success'))
                <div class="alert alert-success">
                    <div class="container">
                        <h5><strong>Fatto! </strong>{{Session::get('success')}}</h5>
                        <a href="/oggetti" class="btn btn-default">Homepage oggetti</a>
                        <a href="/oggetti/{{$oggetto->id}}" class="btn btn-primary">Visualizza libro</a>
                    </div>
                </div>
            @endif
            <div class="panel panel-warning">
                <div class="panel-heading">Modifica dati libro</div>
                <div class="panel-body">
                    <form class="form-horizontal" role="form" method="POST" action="{{ url('/oggetti/aggiorna/'.$oggetto->id.'') }}">
                        {{ csrf_field() }}
                        <input name="_method" type="hidden" value="PUT">
                        @include('oggetti.libro_form')
                        <div class="form-group">
                            <div class="col-md-6 col-md-offset-4">
                                <a href="/oggetti/{{$oggetto->id}}" class="btn btn-danger">Annulla</a>
                                <button type="submit" class="btn btn-warning">Modifica</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection