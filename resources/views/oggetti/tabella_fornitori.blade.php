<div class="panel panel-default ">
    <div class="panel-heading">
        <a href="/oggetti/fornitori/{{$oggetto->id}}" class="pull-right btn btn-default btn-sm glyphicon glyphicon-wrench"></a>
        <h5>Fornitori</h5>
        <h6>Lista dei fornitori di {{$oggetto->nome}}.</h6>
    </div>
    <div class="panel-body">
        <table>
            <tbody>
            @if(count($fornitori) == 0)
                <a href="/oggetti/fornitori/{{$oggetto->id}}" class="btn btn-success btn">Aggiungi fornitori</a>
            @else
                @foreach($fornitori as $fornitore)
                    <tr><td><a href="/fornitori/{{$fornitore->id}}">{{$fornitore->nome}}</a></td></tr>
                @endforeach
            @endif
            </tbody>
        </table>
    </div>
</div>