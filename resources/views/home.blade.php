@extends('layouts.app')

@section('content')
<div class="container">
    <div class="col-md-12">
        <div class="row">
            @include('vendite.recenti')
            @include('ordini.recenti')
        </div>

        <div class="row">
            @include('oggetti.recenti')
            @include('fornitori.recenti')
        </div>
    </div>
</div>

<script>
    $(document).ready(function(){
        $('[data-toggle="tooltip"]').tooltip();
    });
</script>
@endsection
