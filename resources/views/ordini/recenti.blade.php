{{--Ultimi x ordini--}}
<div class="col-md-6 col-sm-6 col-lg-6"  style="cursor: pointer;">
    <div class="panel panel-default">
        <div class="panel-heading">
            <div class="btn-group pull-right">
                <a href="/ordini/" class=" btn btn-sm btn-primary gl-plus"  data-toggle="tooltip" data-delay='{"show":"3000"}' title="Visualizza l'elenco completo degli ordini">visualizza tutti</a>
                <a href="/ordini/nuovo" class="btn btn-sm btn-success gl-plus">nuovo</a>
            </div>
            <h5>Ultimi ordini</h5>
        </div>
        <div class="panel-body">
            <table class="table table-hover">
                <thead><th>Data</th><th>Nome</th><th>Oggetti</th></thead>
                <tbody>
                @foreach($ordini as $ordine)
                    <tr data-toggle="tooltip" data-delay='{"show":"3000"}' title="clicca per visualizzare i dettagli dell'ordine">

                        <td  onclick="window.location='/ordini/{{$ordine->id}}'">
                            {{date('d F Y', strtotime($ordine->data))}}<br>
                            alle {{date('H:i', strtotime($ordine->created_at))}}
                        </td>
                        <td onclick="window:location='/ordini/{{$ordine->id}}'"><b>{{$ordine->nome}}</b></td>
                        <? $oggetti_ordine = $ordine->oggetti()->get(); ?>
                        <td>
                                            <span class="btn btn-sm btn-default" data-toggle="collapse" data-target="#lista{{$ordine->id}}">
                                                <b>{{count($oggetti_ordine)}}</b>
                                                @if(count($oggetti_ordine) >1)
                                                    oggetti
                                                @else
                                                    oggetto
                                                @endif
                                            </span>
                            <div id="lista{{$ordine->id}}" class="collapse">
                                <ul>
                                    @foreach($oggetti_ordine as $oggetto)
                                        <li><a href="/oggetti/{{$oggetto->id}}">{{$oggetto->nome}}</a>  {{$oggetto->pivot->quantita}}x</li>
                                    @endforeach
                                </ul>
                            </div>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>