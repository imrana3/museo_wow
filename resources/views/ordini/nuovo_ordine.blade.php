@extends('layouts.app')
@section('content')
<div class="container">
    <link rel="stylesheet" href="/css/datepicker.css">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-success">
                <div class="panel-heading"><h3 class="panel-title">Nuovo ordine</h3></div>
                <div class="panel-body">
                    <div class="row" id="form_ordine">
                        <div class="col-md-9">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="col-md-8 pull-right col-sm-12">
                                        <input autofocus="true" id="nome" type="text" class="form-control" placeholder="Nome">
                                    </div>
                                    <div class="col-md-12 col-lg-3 col-lg-offset-9">
                                        <input id="data" type="text" class="form-control" placeholder="Data">
                                    </div>
                                </div>
                            </div>
                            <br>

                            <div class="row">
                                <div class="col-md-12">
                                    <div>
                                        <div class="col-md-12">
                                            <input onkeyup="cerca()" autofocus="autofocus" id="search" type="text" class="form-control" placeholder="Cerca un oggetto..">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <br>
                            <div id="errors"></div>
                            <div id="risultati" class="row">
                                @include('oggetti.risultati_ricerca_ordine')
                            </div>
                        </div>

                        <div class="col-md-3">
                            <div class="panel panel-primary">
                                <div class="panel-heading">In ordine</div>
                                <div class="panel-body">
                                    <div id="alertVuoto" class="center-block">
                                        <h5><span class="label label-info">Ordine vuoto. Aggiungi oggetti</span></h5></div>
                                    </div>
                                    <table class="table">
                                        <thead>
                                        <tr><th>Nome</th><th>Qnt.</th><th>Fornitore</th><th></th></tr>
                                        </thead>
                                        <tbody id="body_aggiunti">
                                        </tbody>
                                    </table>
                                </div>

                                <meta name="csrf-token" content="{{ csrf_token() }}">
                                <button onclick="confermaOrdine()" class="btn btn-success btn-lg pull-right">
                                    <i class="fa fa-btn"></i> Conferma
                                </button>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


<script src="/js/datepicker.js"></script>
<script type="text/javascript">

    var lista_oggetti = [];
    var lista_fornitori = [];
    var lista_quantita = [];

    function aggiungi(id, nome){
        if(lista_oggetti.indexOf(""+id) == -1) {
            var fornitore = $('#'+id).find('#fornitore').val();
            var nome_fornitore = $('#'+id).find('#fornitore option:selected').text();
            var quantita = $('#'+id).find('#quantita').val();

            if(quantita <= 0){
                alert('Errore! La quantità ordinata deve essere almeno 1.');
            }
            else {
                lista_oggetti.push("" + id);
                lista_fornitori.push(fornitore);
                lista_quantita.push(quantita);

                var tr = '<tr style="cursor: pointer">' +
                        '<td>' + nome + '</td>' +
                        '<td>' + quantita + '</td>' +
                        '<td>' + nome_fornitore + '</td>' +
                        '<td><button onclick ="togli($(this),' + id + ')" type="button" class="btn btn-sm btn-danger">X</button></td>' +
                        '</tr>;'
                $("#body_aggiunti").append(tr);
            }
        }else{
            alert('oggetto gia presente');
        }


        if(lista_oggetti.length == 1)
            $("#alertVuoto").hide();
    }

    function togli(e, id){
        e.closest('tr').remove();

        var index = lista_oggetti.indexOf(""+id);
        lista_oggetti.splice(index, 1 );
        lista_fornitori.splice(index, 1 );
        lista_quantita.splice(index, 1 );

        if(lista_oggetti.length == 0)
            $("#alertVuoto").show();
    }

    function confermaOrdine() {

        nome = $("#nome").val();
        if (nome.length == 0) {
            $("#errors").html(
                    '<div class="alert alert-dismissible alert-danger"><strong>Errore!</strong> <a href="#" class="alert-link">Assegnare un nome all\'ordine.</div>'
            );
        }
        else{
            if (lista_oggetti.length > 0) {
                post_url = '/ordini/nuovo';
                nome = $("#nome").val();
                data_vendita = $("#data").val();
                $.ajax({
                    url: post_url,
                    type: 'POST',
                    data: {
                        _token: $('meta[name="csrf-token"]').attr('content'),
                        nome: nome,
                        oggetti: lista_oggetti,
                        fornitori: lista_fornitori,
                        quantita: lista_quantita,
                        data_vendita: data_vendita
                    },
                    success: function (data) {
                        $("#form_ordine").html(data);
                    },
                });

            } else {
                $("#errors").html(
                        '<div class="alert alert-dismissible alert-danger"><strong>Errore!</strong> <a href="#" class="alert-link">Lista oggetti vuota!</div>'
                );
            }
        }
    }

    function cerca()
    {
        var query_string = $("#search").val();
        var query_url = '/oggetti/cerca/'+query_string;

        $.ajax({
            url: query_url,
            type: 'GET',
            success: function( data ){
                $("#risultati").html(data.tabella);
            },
            error: function (xhr, b, c) {
                console.log("errore in ajax query");
            }
        });
    }
    $(document).ready(function() {
        params = {
            format: "dd/mm/yyyy",
            autoclose: true,
            locale: 'it'
        };
        $('#data').datepicker(params);
    });
</script>

@if(isset($_GET['q'])){
    <script type="text/javascript">
        $(document).ready(function(){
            q = "{{$_GET['q']}}";
            $("#search").val(q);
            cerca();
        });
    </script>
}
@endif

@endsection