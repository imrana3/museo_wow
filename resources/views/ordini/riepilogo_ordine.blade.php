<div class="col-md-offset-1 col-md-10">
    <h3>Ordine effettuato correttamente.</h3>
    <h4>
        <a href="/ordini" class="btn btn-default">Homepage ordini</a>
        <a href="/ordini/nuovo" class="btn btn-success">Nuovo ordine</a>
    </h4>
    <div class="panel panel-primary">
        <div class="panel-heading">Dettagli</div>
        <div class="panel-body">
            <table class="table">
                <thead>
                <tr><th>Nome</th><th>Fornitore</th><th>Quantita</th><th></th></tr>
                </thead>
                <tbody id="body_aggiunti">
                @for($i = 0; $i < count($oggetti); $i++)
                    <tr style="cursor: pointer">
                        <td>{{$oggetti[$i]}}</td>
                        <td>{{$fornitori[$i]}}</td>
                        <td>{{$quantita[$i]}}</td>
                    </tr>
                @endfor
                </tbody>
            </table>
        </div>
    </div>
</div>