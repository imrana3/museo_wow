@extends('layouts.app')
@section('content')
    <div class="container">
        <div class="row">
            <div id="placeholder"></div>
            <div class="col-sm-12">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h2>{{$ordine->nome}}</h2>
                        <div class="btn-group pull-right">
                            <span onclick="conferma('{{$ordine->id}}', '/ordini/')" class="btn btn-danger btn-md">elimina</span>
                        </div>
                        effettuato da <b>{{$ordine->user->nome}}</b><br>
                        il {{date('d F Y', strtotime($ordine->data))}} alle {{date('H:i', strtotime($ordine->created_at))}}

                    </div>
                    <div class="panel-body">
                        <table class="table table-hover">
                            <thead>
                            <tr>
                                <th>Nome</th>
                                <th>Descrizione</th>
                                <th>Fornitore</th>
                                <th>Quantita</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($oggetti as $oggetto)
                                <tr style="cursor: pointer" onclick="window.location='{{ url("oggetti/".$oggetto->id."") }}'">
                                    <td>{{$oggetto->nome}}</td>
                                    <td>{{$oggetto->descrizione}}</td>
                                    <td>
                                        <a href="{{ url("fornitori/".$oggetto->pivot['id_fornitore']."") }}" >
                                            {{$oggetto->fornitore($ordine->id)->first()->nome}}
                                        </a>
                                    </td>
                                    <td>{{$oggetto->pivot->quantita}}</td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @include('common.elimina_modal')
@endsection