@extends('layouts.app')
@section('content')
<div class="container">
    <link rel="stylesheet" href="/css/datepicker.css">
    <p><span onclick="window.location='/ordini/nuovo'"  type="button" class="btn btn-success btn-lg">Nuovo Ordine</span></p>
    <div id="placeholder"></div>
    <div class="panel panel-default">
        <div class="panel-body">
            <div class="input-group col-lg-6">
                <input value="{{$q['q'] or ""}}" onkeyup="listen()"type="text"  id="search"  class="form-control" placeholder="Cerca ordine..." autofocus value="{{$query or ""}}">
                <span class="input-group-btn">
                        <a onclick="cerca()" href="#" class="btn btn-success btn-md">Cerca</a>
                    @if(!empty($q))
                        <a href="/ordini" class="btn btn-danger btn-md">Annulla ricerca</a>
                    @endif
                    </span>
            </div>
            <p class="input-group date input-daterange col-lg-6">
                <input value="{{$q['dal'] or ""}}" id="data_inizio" type="text" class="form-control" placeholder="Dal..">
                <span class="input-group-addon">-</span>
                <input value="{{$q['al']  or ""}}" id="data_fine" type="text" class="form-control" placeholder="Al..">

                <div id="error_placeholder" style="display: none" class="alert alert-danger" role="alert">...</div>
            </p>
            <p class="col-lg-2 col-lg-offset-10 col-md-12">
                <select id="sort" onchange="sort()" class="form-control" id="fornitore">
                    <option value="data">Ordina per data</option>
                    <option value="nome">Ordina per nome</option>
                </select>
            </p>

            </div>
            <br>
            @if(!empty($q))
                <div class="container">
                    @if($q['q'] != "")
                        <h4>Termine di ricerca: <b>{{$q['q']}}</b></h4>
                    @endif
                    @if(isset($q['dal']) || isset($q['al']))
                        <h5>Ordini effettuati
                        @if(isset($q['dal']))
                            dal <b>{{$q['dal']}}</b>
                        @endif

                        @if(isset($q['al']) )
                            fino al <b>{{$q['al']}}</b>
                        @endif
                    </h5>
                    @endif
                    @if(count($ordini) == 0)
                        <h3>Nessun ordine soddisfa i cretieri di ricerca.</h3>
                    @else
                        <h4>{{$ordini->total()}} risultati trovati.</h4>
                    @endif
                </div>
            @endif
            @if(count($ordini) > 0)
                <table id="ordini_table" class="table table-hover">
                    <thead>
                    <tr>
                        <th>Azioni</th>
                        <th>Data</th>
                        <th>Nome</th>
                        <th>Effettuato da</th>
                        <th>Oggetti</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($ordini as $ordine)
                        <tr style="cursor: pointer">
                            <td>
                                <span onclick="conferma('{{$ordine->id}}', '/ordini/')" class="btn btn-danger btn-sm glyphicon glyphicon-trash"></span>
                            </td>
                            <td  onclick="window.location='/ordini/{{$ordine->id}}'">
                                {{date('d F Y', strtotime($ordine->data))}}<br>
                                alle {{date('H:i', strtotime($ordine->created_at))}}
                            </td>
                            <td onclick="window.location='/ordini/{{$ordine->id}}'">{{$ordine->nome}}</td>
                            <td  onclick="window.location='/ordini/{{$ordine->id}}'"><a href="/utenti/{{$ordine->user->id}}">{{$ordine->user->nome}}</a></td>
                            <? $oggetti = $ordine->oggetti ?>
                            <td>
                                <span class="btn btn-sm btn-default" data-toggle="collapse" data-target="#lista{{$ordine->id}}">oggetti</span>
                                <div id="lista{{$ordine->id}}" class="collapse">
                                    <ul>
                                        @foreach($oggetti as $oggetto)
                                            <li><a href="/oggetti/{{$oggetto->id}}">{{$oggetto->nome}}</a>  {{$oggetto->pivot->quantita}}x</li>
                                        @endforeach
                                    </ul>
                                </div>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
                @if(!empty($q))
                    <div class="pagination">{{$ordini->appends($q)->links()}}</div>
                @else
                    <div class="pagination">{{$ordini->appends('sort', $sortBy)->links()}}</div>
                @endif
            @elseif(!isset($q))
                <div><h3>Non è presente alcun ordine.</h3></div>
            @endif
        </div>
    </div>
</div>

@include('common.elimina_modal')

<script src="/js/datepicker.js"></script>
<script type="text/javascript">
    data_inizio = "";
    data_fine = "";
    function cerca()
    {
        var type = $('#sort').val();
        var data_inizio = $('#data_inizio').val();
        var data_fine = $('#data_fine').val();

        var query_string = $("#search").val();

        var query_url = '/ordini/cerca/?q=' + query_string + "&sort=" + type;
        if (controllaData(data_inizio))
            query_url += "&dal=" + data_inizio;
        if (controllaData(data_fine))
            query_url += "&al=" + data_fine;

        location.href = query_url;

    }

    function controllaData(data){
        if(data.length == 10){
            var reg = /(0[1-9]|[12][0-9]|3[01])[- /.](0[1-9]|1[012])[- /.](19|20)\d\d/;
            if (data.match(reg))
                return true;
        }
        return false;
    }

    function listen() {
        //se preme Invio
        if(event.keyCode == 13)
            cerca();
    }

    function sort() {
        type = $('#sort').val();
        if ('{{isset($q)}}') {
            q = '{{$q['q'] or ""}}';
            dal = '{{$q['dal'] or ""}}';
            al = '{{$q['al'] or ""}}';

            url = '/ordini/cerca/?q=' + q + "&sort=" + type;
            if (q.length > 0)
                url += "&q=" + q;
            if (dal.length > 0)
                url += "&dal=" + dal;
            if (al.length > 0)
                url += "&al=" + al;
        }
        else
            url = '/ordini/?sort='+type;

        $(window.location).attr('href', url);

    }

    $( document ).ready(function() {

        $('#sort').val('{{$q['sort'] or $sortBy}}');
        params = {
            format: "dd/mm/yyyy",
            autoclose:true,
            locale: 'it'
        };
        $('#data_inizio').datepicker(params);
        $('#data_fine').datepicker(params);


    });
</script>
@endsection