<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Ordine extends Model
{
    protected $table = 'ordini';

    public function user(){
        return $this->belongsTo('App\User', 'id_utente');
    }
    public function oggetti(){
        return $this->belongsToMany('App\Oggetto', 'oggetti_ordinati', 'id_ordine', 'id_oggetto')
            ->withPivot('quantita','id_fornitore')
            ->withTimestamps();
    }
}
