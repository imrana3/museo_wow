<?php

//ROUTE PER OGGETTI
Route::group(['middleware' => 'auth', 'prefix' => 'oggetti'], function() {
    Route::get('/', 'OggettiController@index');
    Route::get("cerca/{query}", "OggettiController@cerca");

    Route::get('nuovo', 'OggettiController@inserisci');
    Route::post('libro', 'OggettiController@salvaLibro');
    Route::post('gadget', 'OggettiController@salvaGadget');

    //ACCESSO CONSENTITO SOLO UTENTI CHE GESTISCONO IL MAGAZZINO
    Route::group(['middleware' => ['magazzino']], function() {
        Route::get('modifica/{id}', 'OggettiController@modifica');
        Route::put('aggiorna/{id}', 'OggettiController@aggiorna');
        Route::put('{id}', 'OggettiController@aggiorna');
        Route::delete('{id}', 'OggettiController@elimina');
        Route::get('fornitori/{id}', 'OggettiController@modificaFornitori');
        Route::post('fornitori/{id}', 'OggettiController@aggiornaFornitori');
    });

    Route::get('{id}', 'OggettiController@dettagliOggetto');
});


//ROUTE PER ORDINI
Route::group(['middleware' => 'auth', 'prefix' => 'ordini'], function(){
    Route::get('/', 'OrdiniController@index');
    Route::get("cerca", 'OrdiniController@cerca');

    Route::group(['middleware' => ['magazzino']], function() {
        Route::get('nuovo', 'OrdiniController@nuovoOrdine');
        Route::post('nuovo', 'OrdiniController@inserisciOrdine');
        Route::delete('{id}', 'OrdiniController@elimina');
    });

    Route::get('{id}', 'OrdiniController@dettagliOrdine');
});


//ROUTE PER FORNITORI
Route::group(['middleware' => 'auth', 'prefix' => 'fornitori'], function(){
    Route::get('/', 'FornitoriController@index');
    Route::get("cerca/{query}", 'FornitoriController@cerca');

    Route::group(['middleware' => ['magazzino']], function() {
        Route::get('nuovo', 'FornitoriController@nuovo');
        Route::post('/nuovo', 'FornitoriController@inserisciFornitore');
        Route::get('modifica/{id}', 'FornitoriController@modifica');
        Route::put('aggiorna/{id}', 'FornitoriController@aggiorna');
        Route::delete('{id}', 'FornitoriController@elimina');
    });

    Route::get('{id}', 'FornitoriController@dettagliFornitore');
});


//ROUTE PER HOME
Route::group(['middleware' => 'auth'], function() {

    Route::get('/', 'HomeController@index');
    Route::get('/home', 'HomeController@index');
});


//ROUTE PER VENDITE
Route::group(['middleware' => 'auth', 'prefix' => 'vendite'], function() {

    Route::get('/', 'VenditeController@index');

    Route::get('nuova','VenditeController@nuova');

    Route::get('cerca', 'VenditeController@cerca');

    Route::get('{id}', 'VenditeController@dettagliVendita');

    Route::delete('{id}', 'VenditeController@elimina');
    
    Route::post('nuova', 'VenditeController@inserisciVendita');

});


//ROUTE PER UTENTI E ADMIN
Route::group(['middleware' => ['auth']], function() {
    Route::get('/utenti/{id}', 'UtentiController@profiloUtente');

    Route::get('/utenti/modifica/{id}', 'UtentiController@modifica');
    Route::put('/utenti/modifica/{id}', 'UtentiController@aggiorna');

    //ACCESSO CONSENTITO SOLO AD ADMIN
    Route::group(['middleware' => 'admin', 'prefix' => 'admin'], function() {
        Route::get('/', 'UtentiController@utenti');
        Route::get('cerca/{query}', 'UtentiController@cerca');
        Route::get('registra', 'UtentiController@registra');
        Route::post('registra', 'UtentiController@registraUtente');
        Route::delete('{id}', 'UtentiController@elimina');
    });
});


//ROUTE PER GESTIONE LOGIN
Route::get('/login', 'Auth\AuthController@login');
Route::post('/login', 'Auth\AuthController@performLogin');
Route::get('/logout', 'Auth\AuthController@logout');


