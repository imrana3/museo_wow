<?php

namespace App\Http\Middleware;
use Auth;

use Closure;

class MagazzinoMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if(Auth::user())
            if(Auth::user()->tipologia == 'magazzino')
                return $next($request);

        return response(view('errors.forbidden')
        ->withMessage('Accesso negato!'));
    }
}
