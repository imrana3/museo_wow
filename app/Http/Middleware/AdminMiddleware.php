<?php

namespace App\Http\Middleware;
use Auth;
use Closure;

class AdminMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if(Auth::user())
            if(Auth::user()->tipologia == 'admin')
                return $next($request);

        return response(view('errors.forbidden')
            ->withMessage('Accesso negato!'));
    }
}
