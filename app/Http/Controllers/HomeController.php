<?php

namespace App\Http\Controllers;

use App\Oggetto;
use App\Ordine;
use App\Vendita;
use App\Fornitore;

class HomeController extends Controller
{
    public function index()
    {
        $ultimi_oggetti = Oggetto::orderBy('created_at', 'desc')->take(10)->get();
        $ultimi_ordini = Ordine::orderBy('data', 'desc')->take(10)->get();
        $ultime_vendite = Vendita::orderBy('data', 'desc')->take(10)->get();
        $ultimi_fornitori = Fornitore::orderBy('created_at', 'desc')->take(10)->get();
        return view('home')->with([
           'oggetti' => $ultimi_oggetti,
           'ordini' => $ultimi_ordini,
           'vendite' => $ultime_vendite,
           'fornitori' => $ultimi_fornitori
        ]);
    }
}
