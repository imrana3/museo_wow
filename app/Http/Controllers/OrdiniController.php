<?php

namespace App\Http\Controllers;

use App\Fornitore;
use Illuminate\Http\Request;
use Auth;
use Illuminate\Support\Facades\Input;
use \DateTime;
use App\Ordine;
use App\Oggetto;

class OrdiniController extends Controller
{
    public function index(){
        if(Input::get('sort') == 'data' || Input::get('sort') == 'nome')
            $sortBy = Input::get('sort');
        else
            $sortBy = 'data';

        $ordini = Ordine::orderBy($sortBy, ($sortBy == 'data'? 'DESC' : 'ASC'))->paginate(10);
        return view('ordini.ordini')
            ->with(['ordini' => $ordini, 'sortBy' => $sortBy]);
    }

    public function nuovoOrdine(){

        $ultimi_10 = Oggetto::orderBy('created_at', 'desc')->take(10)->get();
        for($i=0; $i < count($ultimi_10); $i++) {
            $fornitori_oggetto = $ultimi_10[$i]->fornitori_oggetto()->orderBy('nome')->get();
            $id = $ultimi_10[$i]->id;
            $fornitori_rimanenti = Fornitore::whereDoesntHave('oggetti_forniti', function ($q) use ($id) {
                $q->where('id_oggetto', $id);
            })->orderBy('nome')->get();
            $fornitori[$i] = $fornitori_oggetto->merge($fornitori_rimanenti);
        }
//        dd($oggetti_trovati, $fornitori, $fornitori_rimanenti);
        return view('ordini.nuovo_ordine')->with([
            'oggetti_trovati' => $ultimi_10,
            'fornitori' => $fornitori
        ]);
    }

    public function inserisciOrdine(Request $request)
    {
            $ordine = new Ordine;
            $ordine->nome = $request->get('nome');
            if(Input::get('data_vendita') != "") {
                $data = DateTime::createFromFormat('d/m/Y', Input::get('data_vendita') )->format('Y-m-d 00:00:00');
                $ordine->data = $data;
            }
            $ordine->id_utente = Auth::id();
            $ordine->save();

            $oggetti = $request->get('oggetti');
            $fornitori = $request->get('fornitori');
            $quantita = $request->get('quantita');

            for ($i = 0; $i < count($oggetti); $i++) {
                $ordine->oggetti()->attach($oggetti[$i], [
                    'id_fornitore' => $fornitori[$i],
                    'quantita' => $quantita[$i]
                ]);

                $oggetto = Oggetto::find($oggetti[$i]);
                $oggetto->quantita += $quantita[$i];
                $oggetto->save();
            }

            //return view di riepilogo
            $oggetti = Oggetto::find($oggetti)->pluck('nome');
            for($i=0; $i < count($fornitori); $i++){
                $fornitore = Fornitore::find($fornitori[$i]);
                $nomi_fornitori[$i] = $fornitore->nome;
            }

            return view('ordini.riepilogo_ordine')->with([
                'oggetti'   => $oggetti,
                'quantita'  => $quantita,
                'fornitori' => $nomi_fornitori
            ])->render();
    }

    public function cerca(){

        if(Input::get('sort') == 'data' || Input::get('sort') == 'nome')
            $sortBy = Input::get('sort');
        else
            $sortBy = 'data';

        $query = Input::get('q') or "";
        if(Input::has('dal'))
            $dal_date = DateTime::createFromFormat('d/m/Y', Input::get('dal'))->format('Y-m-d 00:00:00');
        else
            $dal_date = "1970-01-01 00:00:00";

        if(Input::has('al'))
            $al_date = DateTime::createFromFormat('d/m/Y', Input::get('al'))->format('Y-m-d 00:00:00');
        else
            $al_date = date("Y-m-d h:m:s");

        if($query != "") {
            $ordini = Ordine::where('nome', 'like', '%' . $query . '%')
                ->where('data', '>=', $dal_date)
                ->where('data', '<=', $al_date)
                ->orwhereHas('oggetti', function ($q) use ($query, $dal_date, $al_date) {

                    $q->where('nome', 'like', '%'.$query.'%')
                        ->where('ordini.data', '>=', $dal_date)
                        ->where('ordini.data', '<=', $al_date);

                    $q->orWhere('barcode', $query)
                        ->where('ordini.data', '>=', $dal_date)
                        ->where('ordini.data', '<=', $al_date);
                });
        }else{
            $ordini = Ordine::where('data', '>=', $dal_date)
                ->where('data', '<=', $al_date);
        }

        if(!isset($ordini))
            return $this->index();

        $ordini = $ordini->orderBy($sortBy, ($sortBy == 'data'? 'DESC' : 'ASC'))
            ->paginate(10);

        return view('ordini.ordini')
            ->with(['ordini'=> $ordini, 'q' => Input::query()]);
    }

    public function dettagliOrdine($id){
        $ordine = Ordine::find($id);
        if($ordine){
            $oggetti = $ordine->oggetti;
            return view('ordini.dettagli')->with(['ordine' =>$ordine, 'oggetti' => $oggetti]);
        }
        return view('errors.404')->with('message', 'Ordine non trovato.');
    }

    public function elimina($id)
    {
        $ordine = Ordine::find($id);
        if ($ordine) {

            //aggiorno le quantità degli oggetti che erano in ordine
            $oggetti = $ordine->oggetti()->get();
            foreach($oggetti as $oggetto){
                $oggetto->quantita -= $oggetto->pivot['quantita'];
                $oggetto->save();
            }

            $ordine->delete();
            return "success";
        } else
            return "errore";
    }
}
