<?php

namespace App\Http\Controllers;

use App\Fornitore;
use App\Libro;
use App\Oggetto;

use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Input;
use Session;
use Redirect;
use Carbon\Carbon;
use View;

class OggettiController extends Controller
{

    //FUNZIONI GESTIONE ROUTES
    public function index()
    {
        $libri = Oggetto::where('id_libro', '<>', 'null')->paginate(10,['*'], 'libri');
        $oggetti = Oggetto::where('produttore', '<>', 'null')->paginate(10,['*'], 'gadget');

        return View::make('oggetti.oggetti', compact('libri', $libri), compact('oggetti',$oggetti));
    }

    public function inserisci()
    {
        $fornitori = Fornitore::orderBy('created_at')->get();
        return view('oggetti.inserisci')->withFornitori($fornitori);
    }

    public function cerca($query)
    {
        if(Input::get('src') == 'libri'){
            $libri = Oggetto::where('id_libro', '<>', 'null')
                ->where('nome', 'like', '%' . $query . '%')
                ->orWhere('barcode', '=', $query)
                ->orWhere('descrizione', 'like', '%' . $query . '%')
                ->paginate(10);

            $tabella = view('oggetti.tabella_libri')
                ->with('libri', $libri)
                ->render();
            return response()->json(array('success' => true, 'tabella' => $tabella));
        }else if(Input::get('src') == 'gadget'){

            $oggetti = Oggetto::where('produttore', '<>', 'null')
                ->where('nome', 'like', '%' . $query . '%')
                ->orWhere('descrizione', 'like', '%' . $query . '%')
                ->paginate(10);

            $tabella = view('oggetti.tabella_oggetti')
                ->with('oggetti', $oggetti)
                ->render();
            return response()->json(array('success' => true, 'tabella' => $tabella));
        }

        $oggetti_trovati = Oggetto::where('nome', 'like', '%' . $query . '%')
            ->orWhere('barcode', '=', $query)
            ->orWhere('descrizione', 'like', '%' . $query . '%')
            ->get();

        if(Input::get('src') == 'vendita') {
            $tabella_oggetti = view('oggetti.risultati_ricerca_vendita')
                ->with('oggetti_trovati', $oggetti_trovati)
                ->render();

        }else{
            for($i=0; $i < count($oggetti_trovati); $i++) {
                $fornitori_oggetto = $oggetti_trovati[$i]->fornitori_oggetto()->orderBy('nome')->get();
                $id = $oggetti_trovati[$i]->id;
                $fornitori_rimanenti = Fornitore::whereDoesntHave('oggetti_forniti', function ($q) use ($id) {
                    $q->where('id_oggetto', $id);
                })->orderBy('nome')->get();
                $fornitori[$i] = $fornitori_oggetto->merge($fornitori_rimanenti);
            }
            $tabella_oggetti = view('oggetti.risultati_ricerca_ordine')
                ->with([
                    'oggetti_trovati' => $oggetti_trovati,
                    'fornitori' => $fornitori
                ])
                ->render();
        }

        return response()->json(array('success' => true, 'tabella' => $tabella_oggetti));
    }

    public function salvaLibro()
    {
        $validator = $this->validateBook();
        if ($validator->fails()) {
            return redirect('oggetti/nuovo/')
                ->withErrors($validator)
                ->withInput();
        }

        $libro = new Libro;
        $this->saveBook($libro);

        $oggetto = new Oggetto;
        $oggetto->barcode = Input::get('barcode');
        $oggetto->id_libro = $libro->id;
        $this->salvaOggetto($oggetto);

        if(Input::has('fornitori'))
            $oggetto->fornitori_oggetto()->attach(Input::get('fornitori'));

        Session::flash('success', 'Libro inserito correttamente nel sistema.');
        Session::flash('id', $oggetto->id);
        return Redirect::back();

    }

    public function salvaGadget()
    {
        $validator = $this->validateGadget();
        if ($validator->fails()) {

            return redirect('/oggetti/nuovo/')
                ->withErrors($validator)
                ->withInput();
        }

        $oggetto = new Oggetto;
        $oggetto->produttore = Input::get('produttore');
        $this->salvaOggetto($oggetto);

        if(Input::has('fornitori'))
            $oggetto->fornitori_oggetto()->attach(Input::get('fornitori'));

        Session::flash('success', 'Gadget inserito correttamente nel sistema');
        Session::flash('id', $oggetto->id);
        return Redirect::back();

    }

    public function modifica($id)
    {
        $oggetto = Oggetto::find($id);
        if($oggetto){
            if ($oggetto->id_libro) {
                return view('oggetti.modifica_libro')
                    ->with('oggetto', $oggetto);
            } else {
                return view('oggetti.modifica_gadget')
                    ->with('oggetto', $oggetto);
            }
        }else{
            return view('errors.404')->with('message', 'Oggetto non trovato.');
        }
    }

    public function aggiorna($id)
    {
        $oggetto = Oggetto::find($id);
        if ($oggetto->id_libro)
            return $this->aggiornaLibro($oggetto, $id);
        else
            return  $this->aggiornaGadget($oggetto, $id);
    }

    public function dettagliOggetto($id)
    {

        $oggetto = Oggetto::find($id);
        if ($oggetto) {
            $vendite = $oggetto->vendite()->orderBy('data', 'DESC')->get()->groupBy(function($date) {
                //return Carbon::parse($date->created_at)->format('Y'); // grouping by years
                return Carbon::parse($date->data)->format('m'); // grouping by months
            });
            $ordini = $oggetto->ordine()->orderBy('data', 'DESC')->get()->groupBy(function($date) {
                //return Carbon::parse($date->created_at)->format('Y'); // grouping by years
                return Carbon::parse($date->data)->format('m'); // grouping by months
            });
            $fornitori = $oggetto->fornitori_oggetto()->get();

            if ($oggetto->id_libro)
                return view('oggetti.dettagli_libro')->with([
                    'oggetto' => $oggetto,
                    'vendite' => $vendite,
                    'ordini' => $ordini,
                    'fornitori' => $fornitori
                ]);
            else
                return view('oggetti.dettagli_gadget')->with([
                    'oggetto' => $oggetto,
                    'vendite' => $vendite,
                    'ordini' => $ordini,
                    'fornitori' => $fornitori
                ]);
        } else
            return view('errors.404')->with('message', 'Oggetto non trovato.');
    }

    public function elimina($id)
    {
        $oggetto = Oggetto::find($id);
        if ($oggetto) {
            if($oggetto->id_libro) {
                $libro = Libro::find($oggetto->id_libro);
                $libro->delete();
            }
            $oggetto->delete();
            return "success";
        } else
            return "errore";
    }

    public function modificaFornitori($id){
        $oggetto = Oggetto::find($id);
        if ($oggetto) {
            $fornitori_oggetto = $oggetto->fornitori_oggetto()->orderBy('nome')->get();
            $fornitori = Fornitore::whereDoesntHave('oggetti_forniti', function($q) use ($id){
                $q->where('id_oggetto', $id);
                })
            ->orderBy('nome')
            ->get();

            return view('oggetti.gestisci_fornitori')->with([
                'id' => $id,
                'nome' => $oggetto->nome,
                'fornitori_oggetto' =>$fornitori_oggetto,
                'fornitori' => $fornitori
            ]);

        } else
            return view('errors.404')->with('message', 'Oggetto non trovato.');
    }

    public function aggiornaFornitori($id){

        $oggetto = Oggetto::find($id);
        $old_fornitori= $oggetto->fornitori_oggetto()->pluck('id_fornitore')->toArray();
        $new_fornitori = Input::get('fornitori');

        if(count($new_fornitori) > 0) {
            if (count($old_fornitori) > 0) {
                $da_inserire = array_diff($new_fornitori, $old_fornitori);
                $da_togliere = array_diff($old_fornitori, $new_fornitori);

                if(count($da_inserire) > 0)
                    $oggetto->fornitori_oggetto()->attach($da_inserire);
                if(count($da_togliere) > 0)
                    $oggetto->fornitori_oggetto()->detach($da_togliere);
            }else
                $oggetto->fornitori_oggetto()->attach($new_fornitori);
        }else if(count($old_fornitori) > 0){
            $oggetto->fornitori_oggetto()->detach($old_fornitori);
        }
        return $this->dettagliOggetto($id);
    }

    //FUNZIONI AUSILIARI

    public function salvaOggetto($oggetto)
    {
        $oggetto->nome = Input::get('nome');
        $oggetto->descrizione = Input::get('descrizione');
        $oggetto->quantita = Input::get('quantita');
        $oggetto->luogo_giacenza = Input::get('luogo_giacenza');
        $oggetto->prezzo_acquisto = Input::get('prezzo_acquisto');
        $oggetto->prezzo_vendita = Input::get('prezzo_vendita');
        $oggetto->iva = Input::get('iva', 0);

        $oggetto->save();
    }

    public function saveBook($libro)
    {

        $libro->nome = Input::get('nome');
        $libro->autore = Input::get('autore');
        $libro->editore = Input::get('editore');
        $libro->save();
    }

    public function aggiornaLibro($oggetto, $id){
        $validator = $this->validateBook();
        if ($validator->fails()) {
            return redirect('oggetti/modifica/'.$id)
                ->withErrors($validator)
                ->withInput();
        }

        $libro = Libro::find($oggetto->id_libro);
        $this->saveBook($libro);

        $oggetto->barcode = Input::get('barcode');
        $this->salvaOggetto($oggetto);

        Session::flash('success', 'Libro aggiornato correttamente nel sistema.');
        return Redirect::back();
    }

    public function aggiornaGadget($oggetto, $id){
        $validator = $this->validateGadget();
        if ($validator->fails()) {

            return redirect('/oggetti/modifica/'.$id)
                ->withErrors($validator)
                ->withInput();
        }

        $oggetto->produttore = Input::get('produttore');
        $this->salvaOggetto($oggetto);

        Session::flash('success', 'Gadget aggiornato correttamente nel sistema.');
        return Redirect::back();
    }

    public function validateGadget(){
        $rules = $this->getRules();
        return Validator::make(Input::all(), $rules, $this->getMessages());
    }

    public function validateBook(){
        $rules = $this->getRules();
        $rules['barcode'] = 'required';

        return Validator::make(Input::all(),$rules, $this->getMessages());
    }

    public function getRules()
    {
        $rules = [
            'nome' => 'required',
            'quantita' => 'required|numeric|min:0',
            'prezzo_acquisto' => 'required|numeric|min:0',
            'prezzo_vendita' => 'required|numeric|min:0',
        ];

        return $rules;
    }

    public function getMessages()
    {
        $messages = [
            'required' => 'Questo campo è obbligatorio',
            'numeric' => 'E\' richiesto un numero',
            'min' => 'Inserire un numero positivo'
        ];

        return $messages;
    }
}