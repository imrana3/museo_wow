<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Input;
use \DateTime;
use Auth;

use \App\Vendita;
use \App\Oggetto;

class VenditeController extends Controller
{
    public function index(){
        $vendite = Vendita::orderBy('data', 'DESC')->paginate(10);
        return view('vendite.vendite')
            ->with('vendite', $vendite);
    }

    public function nuova(){
        $oggetti_trovati = Oggetto::orderBy('created_at', 'desc')->take(10)->get();
        return view('vendite.nuova_vendita')->with('oggetti_trovati', $oggetti_trovati);
    }

    public function inserisciVendita( ){
        if(count(Input::get('oggetti')) > 0 ) {

            $oggetti = Input::get('oggetti');
            $quantita = Input::get('quantita');
            $prezzi = Input::get('prezzi');

            $vendita = new Vendita;
            if(Input::get('data_vendita') != "") {
                $data = DateTime::createFromFormat('d/m/Y', Input::get('data_vendita') )->format('Y-m-d 00:00:00');
                $vendita->data = $data;
            }
            $vendita->id_utente = Auth::id();
            $vendita->save();
            for ($i = 0; $i < count($oggetti); $i++) {
                $vendita->oggetti()->attach($oggetti[$i],
                    ['quantita' => $quantita[$i],
                        'prezzo_applicato' => $prezzi[$i]
                    ]);

                $oggetto = Oggetto::find($oggetti[$i]);
                $oggetto->quantita -= $quantita[$i];
                $oggetto->save();

            }

            $oggetti = Oggetto::find($oggetti)->pluck('nome');
            return view('vendite.riepilogo_vendita')->with([
                'oggetti'   => $oggetti,
                'quantita'  => $quantita,
                'prezzi'    => $prezzi
                ])->render();
        }else{
            return '<div class="alert alert-danger"><strong>Errore!</strong> Lista oggetti vuota!</div>';
        }
    }

    public function cerca(){
        $query = Input::get('q') or "";

        if(Input::has('dal'))
            $dal_date = DateTime::createFromFormat('d/m/Y', Input::get('dal'))->format('Y-m-d 00:00:00');
        else
            $dal_date = "1970-01-01 00:00:00";

        if(Input::has('al'))
            $al_date = DateTime::createFromFormat('d/m/Y', Input::get('al'))->format('Y-m-d 00:00:00');
        else
            $al_date = date("Y-m-d h:m:s");

        if($query != "") {
            $vendite = Vendita::where('vendite.data', '>=', $dal_date)
                ->where('vendite.data', '<=', $al_date)
                ->whereHas('oggetti', function ($q) use ($query, $dal_date, $al_date) {

                    $q->where('nome', 'like', '%'.$query.'%')
                        ->where('vendite.data', '>=', $dal_date)
                        ->where('vendite.data', '<=', $al_date);
                    $q->orWhere('barcode', $query)
                        ->where('vendite.data', '>=', $dal_date)
                        ->where('vendite.data', '<=', $al_date);
                });
        }else{
            $vendite = Vendita::where('data', '>=', $dal_date)
                        ->where('data', '<=', $al_date);
        }

        if(!isset($vendite))
            return $this->index();

        $vendite = $vendite->orderBy('data', 'DESC')->paginate(10);

        return view('vendite.vendite')
            ->with(['vendite'=> $vendite, 'q' => Input::query()]);
    }

    public function dettagliVendita($id){
        $vendita = Vendita::find($id);
        if ($vendita) {
            $oggetti = $vendita->oggetti;
            return view('vendite.dettagli')
                ->with(['vendita' => $vendita, 'oggetti' => $oggetti]);
        }
        else
            return view('errors.404')->with('message', 'Vendita non trovata.');
    }

    public function elimina($id){
        $vendita = Vendita::find($id);
        if($vendita){

            //aggiorno le quantità degli oggetti che erano in vendita
            $oggetti = $vendita->oggetti()->get();
            foreach($oggetti as $oggetto){
                $oggetto->quantita += $oggetto->pivot['quantita'];
                $oggetto->save();
            }

            $vendita->delete();
            return "success";
        }else
            return "error";
    }

}
