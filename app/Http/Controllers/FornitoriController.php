<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Input;
use Session;
use Redirect;

use App\Fornitore;

class FornitoriController extends Controller
{
    //metodi che implementano le route
    public function index(){
        if(Input::get('sort') == 'created_at' || Input::get('sort') == 'nome')
            $sortBy = Input::get('sort');
        else
            $sortBy = 'created_at';
        $fornitori = Fornitore::orderBy($sortBy)->paginate(10);

        return view('fornitori.fornitori')
            ->with(['fornitori'=>$fornitori, 'sortBy'=>$sortBy]);
    }

    public function nuovo(){
        return view('fornitori.inserisci');
    }

    public function cerca($query){

        if(Input::get('sort') == 'created_at' || Input::get('sort') == 'nome')
            $sortBy = Input::get('sort');
        else
            $sortBy = 'created_at';

        $fornitori = Fornitore::where('nome', 'like', '%'.$query.'%')
            ->orWhere('alias', 'like', '%'.$query.'%')
            ->orderBy($sortBy)
            ->paginate(10);

        return view('fornitori.fornitori')
            ->with('fornitori', $fornitori)
            ->with(['query'=> $query, "sortBy" => $sortBy]);
    }

    public function modifica($id){
        $fornitore = Fornitore::find($id);
        if($fornitore)
            return view('fornitori.modifica')->with('fornitore', $fornitore);
        else
            return view('errors.404')->with('message', 'Fornitore non trovato.');
    }

    public function aggiorna($id){
        $validator = Validator::make(Input::all(),$this->getRules(), $this->getMessages());
        if ($validator->fails()) {
            return redirect('fornitori/nuovo')
                ->withErrors($validator)
                ->withInput();
        }

        $fornitore = Fornitore::find($id);

        $this->salvaFornitore($fornitore);

        Session::flash('success', 'Modifiche apportate correttamente.');
        return Redirect::back();
    }
    public function dettagliFornitore($id){
        $fornitore = Fornitore::find($id);
        $oggetti = $fornitore->oggetti()->paginate(10);
        $oggetti_forniti = $fornitore->oggetti_forniti()->get();
        if($fornitore){
            return view('fornitori.dettagli')
                ->with([
                    'fornitore' => $fornitore,
                    'oggetti' => $oggetti,
                    'oggetti_forniti' => $oggetti_forniti
                ]);
        }else{
            return view('errors.404')->with('message', 'Fornitore non trovato.');
        }
    }

    public function inserisciFornitore(){

        $validator = Validator::make(Input::all(),$this->getRules(), $this->getMessages());
        if ($validator->fails()) {
            return redirect('fornitori/nuovo')
                ->withErrors($validator)
                ->withInput();
        }

        $fornitore = new Fornitore;
        $this->salvaFornitore($fornitore);

        Session::flash('success', 'Fornitore inserito correttamente nel sistema.');
        return Redirect::back();
    }

    public function elimina($id)
    {
        $fornitore = Fornitore::find($id);
        if ($fornitore) {
            $fornitore->delete();
            return "success";
        } else
            return "errore";
    }

    //metodi ausiliari
    public function salvaFornitore($fornitore){
        $fornitore->nome = Input::get('nome');
        $fornitore->email = Input::get('email');
        $fornitore->telefono = Input::get('telefono');
        $fornitore->indirizzo = Input::get('indirizzo');
        $fornitore->tipo_contratto = Input::get('tipo_contratto');
        $fornitore->alias = Input::get('alias');

        $fornitore->save();

    }

    public function getRules(){
        return [
            'nome'          => 'required',
            'email'         => 'email',
            'telefono'      => 'numeric|min:5',
            'tipo_contratto'    => 'required'
        ];
    }

    public function getMessages(){
        return [
            'required'      => 'Questo campo è obbligatorio',
            'numeric'       => 'E\' richiesto un numero',
            'email'         => 'Inserire una email valida'
        ];
    }
}
