<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Input;
use Session;
use Redirect;
use Auth;

use App\Http\Controllers\Auth\AuthController;
use App\User;
use App\Ordine;
use App\Vendita;

class UtentiController extends Controller
{
    public function profiloUtente($id){

        $user = User::find($id);

        if(Input::get('src') == 'ordini'){

            $ordini = Ordine::where('id_utente', '=', $user->id)->paginate(10);
            $tabella = view('utenti.tabella_ordini')->with('ordini', $ordini)->render();

            return response()->json(array('success' => true, 'tabella' => $tabella));
        }

        if(Input::get('src') == 'vendite'){
            $vendite = Vendita::where('id_utente', '=', $user->id)->paginate(10);
            $tabella = view('utenti.tabella_vendita')->with('vendite', $vendite)->render();

            return response()->json(array('success' => true, 'tabella' => $tabella));
        }

        $ordini = Ordine::where('id_utente', '=', $user->id)->paginate(10);
        $vendite = Vendita::where('id_utente', '=', $user->id)->paginate(10);

        return view('utenti.profilo')->with([
            'user'  =>  $user,
            'ordini' => $ordini,
            'vendite' => $vendite
            ]);
    }

    public function modifica($id){
        $utente = User::find($id);
        if(Auth::id() == $utente->id || Auth::user()->tipologia == 'admin')
            return view('utenti.modifica')->with('utente', $utente);
        else
            return view('errors.forbidden')->withMessage('Azione non permessa!');
    }

    public function aggiorna($id){

        $rules =  ['nome' => 'required|max:255', 'cognome' => 'required|max:255', 'telefono' => 'required|max:255', 'email' => 'required|max:255'];
        $validator = Validator::make(Input::all(),$rules);
        if($validator->fails()){
            return redirect('/utenti/modifica/'.$id)
                ->withErrors($validator)
                ->withInput();
        }

        $utente = User::find($id);

        if($utente->email != Input::get('email')){
            $validator = Validator::make(array('email'  => Input::get('email')),['email' =>'required|email|max:255|unique:users']);
            if($validator->fails()){
                return redirect('/utenti/modifica/'.$id)
                    ->withErrors($validator)
                    ->withInput();
            }
        }
        $utente->nome = Input::get('nome');
        $utente->cognome = Input::get('cognome');
        $utente->telefono = Input::get('telefono');
        $utente->email = Input::get('email');

        $utente->save();

        Session::flash('success', 'Dati aggiornati correttamente.');
        return Redirect::back();
    }

    public function utenti(){
        $utenti = User::paginate(8);
        return view('utenti.admin_index')->withUtenti($utenti);
    }

    public function cerca($query){
        $utenti = User::where('nome', 'like', '%'.$query.'%')
            ->orWhere('cognome', 'like', '%'.$query.'%')
            ->paginate(8);

        return view('utenti.admin_index')
            ->withUtenti($utenti)
            ->withQuery($query);
    }

    public function elimina($id)
    {
        $user = User::find($id);
        if ($user) {
            $user->delete();
            return "success";
        } else
            return "errore";
    }

    public function registra(){
        return view('auth.register');
    }

    public function registraUtente(){
        $auth = new AuthController;
        $validator = $auth->validator(Input::all());
        if($validator->fails())
            return redirect('/admin/registra/')
                ->withErrors($validator)
                ->withInput();

        $auth->create(Input::all());

        Session::flash('success', 'Utente registrato correttamente nel sistema.');
        return Redirect::to('/admin');
    }
}
