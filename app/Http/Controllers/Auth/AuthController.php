<?php

namespace App\Http\Controllers\Auth;

use App\User;
use Validator;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\ThrottlesLogins;
use Illuminate\Foundation\Auth\AuthenticatesAndRegistersUsers;
use Illuminate\Support\Facades\Input;
use Redirect;

use Auth;
class AuthController extends Controller
{

    /*
    |--------------------------------------------------------------------------
    | Registration & Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users, as well as the
    | authentication of existing users. By default, this controller uses
    | a simple trait to add these behaviors. Why don't you explore it?
    |
    */

    public function login(){
        return view('auth.login');
    }
    /* attempt login with email or username*/
    public function performLogin(){
        $email = Input::get('email');
        $password = Input::get('password');
        $remember = Input::get('remember');
        if ( !Auth::attempt(['email' => $email, 'password' => $password],  $remember))
            if (!Auth::attempt(['username' => $email, 'password' => $password], $remember))
                return Redirect::back()->withErrore('Credenziali di accesso errate!');

        $tipologia = Auth::user()->tipologia;
        switch ($tipologia) {
            case "admin":
                return Redirect::to('/admin');
                break;
            case "magazzino":
                return Redirect::to('/');
                break;
            case "vendite":
                return Redirect::to('/vendite');
                break;
        }
    }

    public function logout(){
        Auth::logout();
        return Redirect::to('/login');
    }
    use AuthenticatesAndRegistersUsers, ThrottlesLogins;

    /**
     * Create a new authentication controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware($this->guestMiddleware(), ['except' => 'logout']);
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    public function validator(array $data)
    {
        return Validator::make($data, [
            'nome' => 'required|max:255',
            'cognome' => 'required|max:255',
            'telefono' => 'required|max:255',
            'email' => 'required|email|max:255|unique:users',

            'username' => 'required|unique:users|max:255',
            'password' => 'required|min:6|confirmed',
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return User
     */
    public function create(array $data)
    {
        return User::create([
            'nome' => $data['nome'],
            'cognome' => $data['cognome'],
            'telefono' => $data['telefono'],
            'email' => $data['email'],
            'tipologia' => $data['tipologia'],
            'username' => $data['username'],
            'password' => bcrypt($data['password']),
        ]);
    }
}
