<?php

namespace App;

use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    protected $fillable = [
        'nome', 'cognome', 'email', 'telefono','tipologia', 'username', 'password',
    ];

    protected $hidden = [
        'password', 'remember_token',
    ];

    public function ordini(){
        return $this->hasMany('App\Ordine','id_utente','id');
    }
}
