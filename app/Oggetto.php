<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Oggetto extends Model
{
    protected $table = 'oggetti';

    public function ordine(){
        return $this->belongsToMany('App\Ordine', 'oggetti_ordinati', 'id_oggetto', 'id_ordine')
            ->withPivot('quantita','id_fornitore');
    }

    public function libro(){
        return $this->hasOne('App\Libro', 'id', 'id_libro');
    }

    public function vendite(){
        return $this->belongsToMany('App\Vendita', 'oggetti_venduti', 'id_oggetto', 'id_vendita')
            ->withPivot('quantita','prezzo_applicato');
    }

    public function fornitore($id_ordine){
        return $this->belongsToMany('App\Fornitore', 'oggetti_ordinati', 'id_oggetto', 'id_fornitore')
            ->wherePivot('id_ordine', $id_ordine);
    }

    public function fornitori_oggetto(){
        return $this->belongsToMany('App\Fornitore', 'fornitori_oggetto', 'id_oggetto', 'id_fornitore')
            ->withTimestamps();
    }

}
