<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Vendita extends Model
{
    protected $table = 'vendite';

    public function oggetti(){
        return $this->belongsToMany('App\Oggetto', 'oggetti_venduti', 'id_vendita', 'id_oggetto')
            ->withPivot('quantita','prezzo_applicato')
            ->withTimestamps();
    }

    public function user(){
        return $this->belongsTo('App\User', 'id_utente');
    }
}
