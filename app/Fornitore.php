<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Fornitore extends Model
{
    protected $table = 'fornitori';

    public function oggetti(){
        return $this->belongsToMany('App\Oggetto', 'oggetti_ordinati', 'id_fornitore', 'id_oggetto')
            ->withPivot('quantita','id_ordine')
            ->withTimestamps();
    }

    public function oggetti_forniti(){
        return $this->belongsToMany('App\Oggetto', 'fornitori_oggetto', 'id_fornitore', 'id_oggetto')
            ->withTimestamps();
    }
}
